﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "AnswerObject", menuName = "Rules/Answer/GloveAnswer")]

public class GloveAnswerObject : AnswerObject
{
    public string ASTM;
    public string classCode;
    public string classType;
    public int maxUseVolt;
    public Color backgroundColor;
    public Sprite icon;
    public Color textColor = Color.white;
}
