﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    [CreateAssetMenu(fileName = "RuleObject", menuName = "Rules/Rule")]
    public class RuleObject : ScriptableObject
    {
        public string description;
        public List<StepObject> steps;
    }
}