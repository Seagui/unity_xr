﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    [CreateAssetMenu(fileName = "QuestionObject", menuName = "Rules/Question")]
    public class QuestionObject : ScriptableObject
    {
        public string title;
        public Sprite icon;
        public AnswerBehaviour answerPrefab;
        public List<AnswerObject> answers;

        public bool CheckAnswer(AnswerObject answer)
        {
            return answers.Exists(a => a == answer && a.isCorrect);
        }
    }
}