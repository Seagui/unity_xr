﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AnswerObject", menuName = "Rules/Answer/CarpetAnswer")]

public class CarpetAnswerObject : AnswerObject
{
    public string classCode;
    public int volts;
    public Color backgroundColor;
}
