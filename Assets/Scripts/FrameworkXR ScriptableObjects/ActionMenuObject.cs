﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    [CreateAssetMenu(fileName = "ActionMenuObject", menuName = "Rules/ActionMenuObject")]
    public class ActionMenuObject : QuestionObject
    {
        public Sprite icon;
        public List<ActionMenuItemObject> answersItemsInCorrectOrder;

        public int nextAnswer = 0;

        public void NextAnswer()
        {
            if (nextAnswer > 0)
                answersItemsInCorrectOrder[nextAnswer-1].answered = true;

            foreach (ActionMenuItemObject answer in answersItemsInCorrectOrder)
            {
                answer.isCorrect = false;
                if (answersItemsInCorrectOrder.IndexOf(answer) == nextAnswer)
                    answer.isCorrect = true;
                if (answer.answered)
                {
                    answer.textColor = Color.green;
                    answer.iconSetted = answer.iconCorrect;
                }
            }

            nextAnswer++;
        }
    }
}