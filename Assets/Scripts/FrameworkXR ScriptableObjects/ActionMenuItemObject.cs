﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AnswerObject", menuName = "Rules/Answer/ActionMenuItem")]

public class ActionMenuItemObject : AnswerObject
{
    public Sprite icon;
    public Sprite iconCorrect;
    public Sprite iconIncorrect;
    public bool answered;

    [HideInInspector]
    public Color textColor;
    [HideInInspector]
    public Sprite iconSetted;
}