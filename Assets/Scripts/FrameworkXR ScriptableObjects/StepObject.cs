﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    [CreateAssetMenu(fileName = "StepObject", menuName = "Rules/Step")]
    public class StepObject : ScriptableObject
    {
        public bool Completed = false;
        public Sprite icon;
        public List<TaskObject> tasks;
        public bool showInStatus = true;
        public string title = "";

        private bool IsCompleted()
        {
            if (!HasTasks())
                return false;

            return !tasks.Exists(t => !t.Completed);
        }

        public bool StepValid(TaskObject task)
        {
            return tasks.Contains(task);
        }

        public CheckTaskResult CheckTask(TaskObject task)
        {
            CheckTaskResult result = new CheckTaskResult();

            if (!task)
            {
                result.Error = LanguageSystem.Instance.GetValue("task.error.null");
                Debug.LogError(result.Error);
                return result;
            }

            if (!HasTasks())
            {
                result.Error = LanguageSystem.Instance.GetValue("step.error.tasks.empty");
                return result;
            }

            if (task.Completed)
            {
                result.Error = LanguageSystem.Instance.GetValue("task.error.alreadycompleted", task.title);
                Debug.LogWarning(result.Error);
                return result;
            }


            if (tasks.Contains(task))
            {
                if (task.dependsOn && !task.dependsOn.Completed)
                {
                    result.LoseLives = true;
                    result.Error = LanguageSystem.Instance.GetValue("task.error.dependson", new string[] {task.title, task.dependsOn.title});
                    Debug.LogWarning($"The task {task.title} depends on the task {task.dependsOn.title}");
                    return result;
                }

                result.Success = true;
                Debug.Log($"The task {task.name} is in the step {this.name}");
            }
            else
            {
                result.LoseLives = true;
                string stepTitle = SearchStep(task);
                result.Error = LanguageSystem.Instance.GetValue("task.error.invalidstep", new string[] { this.title, stepTitle });
                Debug.LogWarning($"The task {task.title} is not from the current step {this.title}");
                if (task.orderCritical)
                    task.SetSelected();
            }

            result.Error = task.ErrorMessage.Equals("default") ? result.Error : task.ErrorMessage;

            return result;
        }

        string SearchStep(TaskObject task)
        {
            foreach (StepObject step in GameObject.FindGameObjectWithTag("RuleManager").GetComponent<RuleManager>().rule.steps)
                if (step.tasks.Contains(task))
                    return step.title;

            return "Inexistente";
        }

        public CheckTaskResult CompleteTask(TaskObject task)
        {
            CheckTaskResult result = CheckTask(task);

            if (!result.Success)
            {
                Debug.LogError($"The task can not be completed");
                return result;
            }

            task.Completed = true;
            Debug.Log($"The task {task.name} is completed");

            result.Success = true;

            Completed = IsCompleted();

            return result;
        }

        private bool HasTasks()
        {
            if (tasks == null || tasks.Count == 0)
            {
                Debug.LogError("The step has not tasks");
                return false;
            }

            return true;
        }
    }
}