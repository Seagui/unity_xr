﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    public enum TaskObjectType
    {
        Interactable
    }

    [CreateAssetMenu(fileName = "TaskObject", menuName = "Rules/Task")]
    public class TaskObject : ScriptableObject
    {
        public QuestionObject question;
        public bool Completed;
        public TaskObject dependsOn;
        public TaskObjectType taskType;
        public string ErrorMessage = "default";
        public bool orderCritical;
        public bool falsePositiveCritical;
        public string title = "";

        public bool selectedAheadOfTime = false;
        public bool selectedFalsePositive = false;

        void Awake()
        {
            selectedAheadOfTime = false;
            selectedFalsePositive = false;
        }

        public void SetSelected()
        {
            Debug.Log("Y");
            if (orderCritical)
            {
                selectedAheadOfTime = true;
            }
            if (falsePositiveCritical)
            {
                Debug.Log("Z");
                selectedFalsePositive = true;
            }
        }

        public bool GetSelectedAheadOfTime()
        {
            return selectedAheadOfTime;
        }

        public bool GetSelectedFalsePositive()
        {
            return selectedFalsePositive;
        }
    }
}
