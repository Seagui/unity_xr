﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{

    [CreateAssetMenu(fileName = "TaskObjectTutorial", menuName = "Rules/TaskObjectTutorial")]
    public class TaskObjectTutorial : TaskObject
    {
        public string title = "";
        public string text = "";
        public string note = "";
        public bool dontShowText = false;
    }
}
