﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AnswerObject", menuName = "Rules/Answer/SimpleAnswer")]
public class AnswerObject : ScriptableObject
{
    public int id;
    public string title;
    public bool isCorrect;
}
