﻿using OT.Ternium.XR.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CriticalStepsUI : MonoBehaviour
{

    [SerializeField]
    private GameObject criticalStepPrefab;
    [SerializeField]
    private float ySpaceBetweenSteps = -11f;
    [SerializeField]
    private List<TaskObject> criticalTasks;
    [SerializeField]
    private Sprite correctIcon;
    [SerializeField]
    private Sprite incorrectIcon;
    [SerializeField]
    private Color correctColor;
    [SerializeField]
    private Color incorrectColor;

    public void ShowCriticalSteps()
    {

        RectTransform stepTranform = gameObject.GetComponent<RectTransform>();
        Vector2 anchoredPositionOriginal = stepTranform.anchoredPosition;

        foreach (TaskObject task in criticalTasks)
        {
            GameObject stepObject = Instantiate(criticalStepPrefab, transform);
            CriticalStepUI step = stepObject.GetComponent<CriticalStepUI>();
            stepTranform.anchoredPosition = new Vector2(stepTranform.anchoredPosition.x, stepTranform.anchoredPosition.y + ySpaceBetweenSteps);
            step.gameObject.GetComponent<RectTransform>().anchoredPosition = stepTranform.anchoredPosition;

            if (task.GetSelectedAheadOfTime() || task.GetSelectedFalsePositive())
            {
                step.icon.sprite = incorrectIcon;
                step.icon.color = incorrectColor;
            }
            else
            {
                step.icon.sprite = correctIcon;
                step.icon.color = correctColor;
            }

            step.text.text = task.title;
        }

        stepTranform.anchoredPosition = anchoredPositionOriginal;

    }

}