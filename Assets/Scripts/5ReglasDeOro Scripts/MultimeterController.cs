﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    public class MultimeterController : MonoBehaviour
    {
        public Pole _leftObject;
        public Pole _rightObject;

        [SerializeField]
        private MultimeterObject _multimeterObject;
        public UI_System uiSystem;
        public UI_Screen messageScreen;
        public UIMultimeterHelper uiMultimeter;
        void Update()
        {
            if (_leftObject && _rightObject)
            {
                _multimeterObject.tension = _leftObject.tension + _rightObject.tension;
            }
        }
        public void OnLeftRayCastEnter(Transform target)
        {
            _leftObject = target.gameObject.GetComponent<Pole>();
        }
        public void OnLeftRayCastExit(Transform target)
        {
            _leftObject = null;
            _multimeterObject.tension = 0f;
        }
        public void OnRightRayCastEnter(Transform target)
        {
            _rightObject = target.gameObject.GetComponent<Pole>();
        }
        public void OnRightRayCastExit(Transform target)
        {
            _rightObject = null;
            _multimeterObject.tension = 0f;
        }

        public void ShowValue()
        {
            Debug.Log($"Tension between {this._leftObject.gameObject.name} and {this._rightObject.gameObject.name} = {this._multimeterObject.tension}");
            //if (messageScreen)
            //{
            //    MessagesUI messageUi = messageScreen.GetComponent<MessagesUI>();
            //    messageUi.CleanMessage();
            //    messageUi.ShowInfo("Tension", $"Tension = {this._multimeterObject.tension}");
            //    uiSystem.SwitchScreen(messageScreen);
            //}

            if (uiMultimeter)
            {
                uiMultimeter.ShowDoneMessage();
            }
        }
    }
}