﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    public class PlayerPicker : MonoBehaviour
    {
        public Transform pickableZoneRight;
        public Transform pickableZoneLeft;
        public GameObject pickedObjectRight;
        public GameObject pickedObjectLeft;

        public bool HasObjectTaken => pickedObjectLeft || pickedObjectRight;
        private GameObject _orginalObject;
        public void PickUpObject(GameObject pickableObject)
        {
            if (pickableObject.transform.childCount > 0)
            {
                GameObject objRight = null;
                GameObject objLeft = null;

                foreach (Transform pickableTransform in pickableObject.transform)
                {
                    if (pickableTransform.gameObject.tag == "PickableRight")
                    {
                        objRight = pickableTransform.gameObject;
                    }
                    else if (pickableTransform.gameObject.tag == "PickableLeft")
                    {
                        objLeft = pickableTransform.gameObject;
                    }
                }

                if (pickableZoneRight && objRight)
                {
                    objRight.SetActive(true);
                    if (GameLogic.headDevice.isValid)
                    {
                        pickableZoneRight.localRotation = Quaternion.identity;
                        pickableZoneRight.localPosition = Vector3.zero;
                    }

                    Pickup(objRight, ref pickedObjectRight, pickableZoneRight);

                    if (GameLogic.headDevice.isValid)
                    {
                        /*
                        SteamVR_Skeleton_Poser skeletonPoser = objRight.GetComponent<SteamVR_Skeleton_Poser>();

                        if (skeletonPoser != null)
                        {
                            //Player.instance.rightHand.skeleton.BlendToPoser(skeletonPoser);
                        }
                        else
                        {
                            Debug.Log("Objeto " + objRight.name + " no contiene Skeleton Poser script");
                        }
                        */

                        ToolOffsetPosition toolOffset = objRight.GetComponent<ToolOffsetPosition>();

                        if (toolOffset != null)
                        {


                            pickableZoneRight.localRotation = toolOffset.offset.localRotation;
                            pickableZoneRight.localPosition = toolOffset.offset.localPosition;

                            //pickableZoneRight.SetPositionAndRotation(toolOffset.offset.position, toolOffset.offset.rotation);
                            // pickableZoneRight.transform.rotation = Quaternion.Euler(toolOffset.rotation);
                            //pickableZoneRight.transform.position = toolOffset.position;
                        }
                    }
                }

                if (pickableZoneLeft && objLeft)
                {
                    objLeft.SetActive(true);

                    if (GameLogic.headDevice.isValid)
                    {
                        pickableZoneLeft.localRotation = Quaternion.identity;
                        pickableZoneLeft.localPosition = Vector3.zero;
                    }
                    Pickup(objLeft, ref pickedObjectLeft, pickableZoneLeft);


                    if (GameLogic.headDevice.isValid)
                    {
                        /*
                        SteamVR_Skeleton_Poser skeletonPoser = objLeft.GetComponent<SteamVR_Skeleton_Poser>();

                        if (skeletonPoser != null)
                        {
                            //skeletonPoser.
                            Player.instance.leftHand.skeleton.BlendToPoser(skeletonPoser);

                            //Player.instance.leftHand.skeleton.mirroring = SteamVR_Behaviour_Skeleton.MirrorType.RightToLeft;
                        }
                        else
                        {
                            Debug.Log("Objeto " + objLeft.name + " no contiene Skeleton Poser script");
                        }
                        */

                        ToolOffsetPosition toolOffset = objLeft.GetComponent<ToolOffsetPosition>();

                        if (toolOffset != null)
                        {


                            pickableZoneLeft.localRotation = toolOffset.offset.localRotation;
                            pickableZoneLeft.localPosition = toolOffset.offset.localPosition;

                            //pickableZoneRight.SetPositionAndRotation(toolOffset.offset.position, toolOffset.offset.rotation);
                            // pickableZoneRight.transform.rotation = Quaternion.Euler(toolOffset.rotation);
                            //pickableZoneRight.transform.position = toolOffset.position;
                        }
                    }
                }
                pickableObject.SetActive(false);
            }
            else
            {
                Pickup(pickableObject, ref pickedObjectRight, pickableZoneRight);
            }

            _orginalObject = pickableObject;

            /*
            var raycastManager = pickableObject.GetComponent<RayCastManager>();
            if (raycastManager)
                RiskPerceptionPlayer.instance.EnableLeftPointer(raycastManager.enableLeftRayCast);
            */
        }

        private void Pickup(GameObject pickableObject, ref GameObject pickedObject, Transform pickableZone)
        {
            pickedObject = pickableObject;
            pickedObject.transform.SetParent(pickableZone.transform);
            pickedObject.transform.position = pickableZone.transform.position;

        }
        public void ReleaseObject()
        {
            if (pickedObjectRight)
            {
                ReleasePositionBehaviour releasePosition = pickedObjectRight.GetComponent<ReleasePositionBehaviour>();
                if (releasePosition)
                {
                    releasePosition.Release();
                }
                else
                {
                    pickedObjectRight.transform.SetParent(null);
                }
                pickedObjectRight = null;

                /*
                if (Player.instance.rightHand.skeleton != null)
                {
                    Debug.Log("Saco la pose actual<<<<<<<<<<<<<<<<<<<<<");
                    Player.instance.rightHand.skeleton.BlendToSkeleton();
                }
                */

            }
            if (pickedObjectLeft)
            {
                ReleasePositionBehaviour releasePosition = pickedObjectLeft.GetComponent<ReleasePositionBehaviour>();
                if (releasePosition)
                {
                    releasePosition.Release();
                }
                else
                {
                    pickedObjectLeft.transform.SetParent(null);
                }
                pickedObjectLeft = null;

                /*
                if (Player.instance.leftHand.skeleton != null)
                {
                    Debug.Log("Saco la pose actual<<<<<<<<<<<<<<<<<<<<<");
                    Player.instance.leftHand.skeleton.BlendToSkeleton();
                }
                */

            }

            _orginalObject = null;
            //RiskPerceptionPlayer.instance.EnableLeftPointer(false);
        }
    }
}