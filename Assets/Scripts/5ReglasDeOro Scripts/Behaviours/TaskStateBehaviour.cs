﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace OT.Ternium.XR.Framework
{
    public class TaskStateBehaviour : TaskBehaviour
    {

        [SerializeField]
        private GameObject initialState;
        [SerializeField]
        private GameObject finalState;

        public UnityEvent stateChanged;

        public void ChangeState()
        {
            if (initialState)
                initialState.SetActive(false);
            if (finalState)
            {
                finalState.SetActive(true);
                BaseAnimatorBehaviour baseAnimator = finalState.GetComponent<BaseAnimatorBehaviour>();
                if (baseAnimator)
                {
                    baseAnimator.Animate();
                }

                /*
                SteamVR_Skeleton_Poser sPoser = gameObject.GetComponent<SteamVR_Skeleton_Poser>();
                if (sPoser != null)
                {
                    Player.instance.rightHand?.skeleton?.BlendToSkeleton();
                    Player.instance.leftHand?.skeleton?.BlendToSkeleton();
                }
                */

            }
            stateChanged.Invoke();
        }
    }
}