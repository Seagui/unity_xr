﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    public class TaskInteractableBehaviour : TaskBehaviour
    {
        [SerializeField]
        private TaskStateBehaviour taskInteractor;
        public bool CanInteractWith(TaskBehaviour task)
        {
            return taskInteractor == task;
        }
    }
}