﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReleasePositionBehaviour : MonoBehaviour
{
    [SerializeField] private Transform releasePosition;
    [SerializeField] private GameObject visibleObject;
    public void Release()
    {
        if(releasePosition)
        {
            transform.SetParent(releasePosition);
            transform.position = releasePosition.position;
            gameObject.SetActive(false);
            visibleObject.SetActive(true);        
        }
        else
        {
            transform.SetParent(null);
        }
    }
}
