﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAnimatorBehaviour : BaseAnimatorBehaviour
{
    public override void Animate()
    {
        Animator animator = GetComponent<Animator>();
        if(animator)
        {
            animator.SetBool("isOpen", true);
        }
    }
}
