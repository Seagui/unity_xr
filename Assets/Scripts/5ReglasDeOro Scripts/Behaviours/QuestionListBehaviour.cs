﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace OT.Ternium.XR.Framework
{
    public class QuestionListBehaviour : MonoBehaviour
    {
        public QuestionObject question;
        public Text questionName;
        public Image icon;
        public Transform answersPanel;
        public UnityEvent onAnsweredQuestion;
        public UnityEvent onAnswerIncorrect;
        // Start is called before the first frame update
        void Start()
        {
            if (question)
                Display(question);
        }

        public virtual void Display(QuestionObject question)
        {
            this.question = question;
            Refresh();
        }

        public virtual void Refresh()
        {
            questionName.text = question.title;
            if (icon)
                icon.sprite = question.icon;

            foreach (Transform item in answersPanel)
            {
                Destroy(item.gameObject);
            }

            int id = 1;
            question.answers.ForEach(answer =>
            {
                answer.id = id++;
                AnswerBehaviour prefab = AnswerBehaviour.Instantiate(question.answerPrefab, answersPanel);
                prefab.onSelected.AddListener(AnswerSelected);
                prefab.Display(answer);
            });
        }

        private void AnswerSelected(AnswerBehaviour answer)
        {
            if (answer.answer.isCorrect)
            {
                Debug.Log($"Answer {answer.answer.name} is correct");
                onAnsweredQuestion.Invoke();
            }
            else
            {
                Debug.LogError($"Answer {answer.answer.name} is incorrect");
                onAnswerIncorrect.Invoke();
            }
        }
    }

}