﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    public class ColliderControllerBehaviour : MonoBehaviour
    {
        public Collider collider;
        public TaskObject taskCompleteValidation;

        public void Enable(bool enable)
        {
            if (taskCompleteValidation)
            {
                if (taskCompleteValidation.Completed)
                {
                    collider.enabled = enable;
                }
            }
            else
            {
                collider.enabled = enable;
            }
        }
    }
}