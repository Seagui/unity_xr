﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{
    public class GroundCableBehaviour : TaskBehaviour
    {
        public Animator cableAnimator;
        private int cableIndex;
        public override void Click(XRBaseInteractor interactor)
        {
            NextAnimation();
            if (cableIndex > 3)
            {
                base.onSelected.Invoke(this);
            }
        }
        public void NextAnimation()
        {
            switch (cableIndex)
            {
                case 0:
                    cableAnimator.SetBool("canAdvanceToFirst", true);
                    break;
                case 1:
                    cableAnimator.SetBool("canAdvanceToSecond", true);
                    break;
                case 2:
                    cableAnimator.SetBool("canAdvanceToThird", true);
                    break;
                case 3:
                    cableAnimator.SetBool("canAdvanceToFourth", true);
                    break;
            }
            cableIndex++;
        }
    }

}