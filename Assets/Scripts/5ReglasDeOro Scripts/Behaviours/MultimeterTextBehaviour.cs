﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace OT.Ternium.XR.Framework
{
    public class MultimeterTextBehaviour : MonoBehaviour
    {
        public Text text;
        public MultimeterObject multimeterObject;

        // Update is called once per frame
        void Update()
        {
            // Vector3 pos = Camera.main.WorldToScreenPoint(this.transform.position);
            // text.transform.position = pos;
            text.text = $"{multimeterObject.tension} V";
        }
    }
}