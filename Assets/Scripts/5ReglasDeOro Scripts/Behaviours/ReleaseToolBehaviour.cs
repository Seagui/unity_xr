﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    public class ReleaseToolBehaviour : MonoBehaviour
    {
        [SerializeField] private PlayerPicker playerPicker;
        public void Release()
        {
            if (playerPicker.HasObjectTaken)
            {
                playerPicker.ReleaseObject();
            }
        }
    }
}