﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    public class StepListBehaviour : MonoBehaviour
    {
        public List<StepObject> steps;
        public StepBehaviour stepBehaviour;
        public Transform stepPanel;
        public TaskListBehaviour taskListBehaviour;

        private void Start()
        {
            if (steps != null)
                Display(steps);
        }

        public virtual void Display(List<StepObject> stepObjects)
        {
            this.steps = stepObjects;
            Refresh();
        }

        public virtual void Refresh()
        {
            foreach (Transform item in stepPanel)
            {
                Destroy(item.gameObject);
            }

            steps.ForEach(step =>
            {
                StepBehaviour prefab = StepBehaviour.Instantiate(stepBehaviour, stepPanel);
                prefab.onSelected.AddListener(StepSelected);
                prefab.Display(step);
            });
        }

        private void StepSelected(StepBehaviour stepBehaviour)
        {
            Debug.Log(stepBehaviour.step.name);
            taskListBehaviour.Display(stepBehaviour.step.tasks);
        }
    }
}