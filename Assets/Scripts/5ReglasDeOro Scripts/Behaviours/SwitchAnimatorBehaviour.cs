﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchAnimatorBehaviour : BaseAnimatorBehaviour
{
    public override void Animate()
    {
        Animator animator = GetComponent<Animator>();
        if(animator)
        {
            animator.SetBool("isOff", true);
        }
    }
}
