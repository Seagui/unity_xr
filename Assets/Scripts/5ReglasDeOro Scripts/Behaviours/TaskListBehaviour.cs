﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    public class TaskListBehaviour : MonoBehaviour
    {
        public List<TaskObject> tasks;
        public TaskBehaviour taskBehaviour;
        public QuestionListBehaviour questionListBehaviour;
        public Transform taskPanel;

        private void Start()
        {
            if (tasks != null)
                Display(tasks);
        }

        public virtual void Display(List<TaskObject> stepObjects)
        {
            this.tasks = stepObjects;
            // Refresh();
        }

        // public virtual void Refresh()
        // {
        //     foreach (Transform item in taskPanel)
        //     {
        //         Destroy(item.gameObject);
        //     }

        //     tasks.ForEach(task =>
        //     {
        //         TaskBehaviour prefab = TaskBehaviour.Instantiate(taskBehaviour, taskPanel);
        //         prefab.onSelected.AddListener(TaskSelected);
        //     });
        // }

        private void TaskSelected(TaskBehaviour taskBehaviour)
        {
            Debug.Log(taskBehaviour.task.name);
            // questionListBehaviour.Display(taskBehaviour.task.question);
        }
    }

}