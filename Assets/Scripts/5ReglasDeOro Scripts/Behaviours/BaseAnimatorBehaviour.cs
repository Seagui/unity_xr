using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseAnimatorBehaviour : MonoBehaviour
{
    public virtual void Animate()
    {
        
    }
}
