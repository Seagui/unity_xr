﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{
    public class Pole : TaskBehaviour
    {
        public MultimeterController multimeter;
        public float tension;
        public bool finishOnFirstOne;
        public List<Pole> poles;
        private Dictionary<Pole, bool> _poleStates;

        private static bool leftMultimeterCatched = false;
        private static bool rightMultimeterCatched = false;

        private void Awake()
        {
            _poleStates = new Dictionary<Pole, bool>();
            poles.ForEach(p => _poleStates.Add(p, false));
        }

        private void Update()
        {
            if (task.Completed)
            {
                TaskInteractableBehaviour taskIntercat = GetComponent<TaskInteractableBehaviour>();
                if (taskIntercat)
                {
                    taskIntercat.enabled = true;
                    enabled = false;
                }
            }
        }
        public override void Click(XRBaseInteractor interactor)
        {
            if (multimeter && multimeter._leftObject && multimeter._rightObject && multimeter._leftObject != multimeter._rightObject)
            {
                if (multimeter._leftObject == this)
                {
                    if (_poleStates.ContainsKey(multimeter._rightObject))
                    {
                        leftMultimeterCatched = true;
                        //multimeter.ShowValue();
                        multimeter._rightObject.Complete(this);
                        Complete(multimeter._rightObject);
                    }
                }
                else if (multimeter._rightObject == this)
                {
                    if (_poleStates.ContainsKey(multimeter._leftObject))
                    {
                        rightMultimeterCatched = true;
                        //multimeter.ShowValue();
                        multimeter._leftObject.Complete(this);
                        Complete(multimeter._leftObject);
                    }
                }
            }
        }

        public override void Error(string message)
        {
            if (multimeter && multimeter._leftObject && multimeter._rightObject && multimeter._leftObject != multimeter._rightObject)
            {
                if (multimeter._leftObject == this)
                {
                    leftMultimeterCatched = true;
                }
                else if (multimeter._rightObject == this)
                {
                    rightMultimeterCatched = true;
                }
            }

            if (rightMultimeterCatched && leftMultimeterCatched)
            {
                rightMultimeterCatched = false;
                leftMultimeterCatched = false;
                base.Error(message);
            }

        }

        public void Complete(Pole pole)
        {
            if (_poleStates.ContainsKey(pole))
            {
                _poleStates[pole] = true;
                if (leftMultimeterCatched && rightMultimeterCatched)
                {
                    multimeter.ShowValue();
                    rightMultimeterCatched = false;
                    leftMultimeterCatched = false;

                    if (finishOnFirstOne)
                    {
                        Debug.Log("Finished on first one");
                        base.onSelected.Invoke(this);
                    }
                    else if (AreAllPolesCompleted())
                    {
                        bool allCompleted = true;
                        foreach (var ps in _poleStates)
                        {
                            if (!ps.Key.AreAllPolesCompleted())
                            {
                                allCompleted = false;
                                break;
                            }
                        }
                        if (allCompleted)
                        {
                            Debug.Log("All poles are complete");
                            base.onSelected.Invoke(this);
                        }
                    }
                }
            }
        }

        public bool AreAllPolesCompleted()
        {
            return !_poleStates.Any(ps => ps.Value == false);
        }

    }
}