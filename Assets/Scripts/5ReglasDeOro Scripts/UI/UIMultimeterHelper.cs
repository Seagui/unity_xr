﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMultimeterHelper : MonoBehaviour
{

    public Text txtDone;
    public float timeMessageShowed = 0.5f;

    private int idMessage = 0;
    // Start is called before the first frame update
    void Start()
    {
        txtDone.enabled = false;
    }

    public void ShowDoneMessage()
    {

        idMessage++;
        StartCoroutine(ControllMessageShowen(idMessage));
    }

    private IEnumerator ControllMessageShowen(int idMsg)
    {

        if (!txtDone.enabled)
        {
            txtDone.enabled = true;
        }

        yield return new WaitForSeconds(timeMessageShowed);

        if (idMsg == idMessage)
        {
            txtDone.enabled = false;
        }

    }
}
