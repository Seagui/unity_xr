﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace TESSA.RiskPerception.UI
{
    public class EnergyBar : MonoBehaviour
    {
        public Text totalEnergyLabel;
        public Text currentEnergyLabel;
        public Image segment;

        [Tooltip("En caso de solaparse el rango tienen prioridad por orden en la lista.\nDe no encontrar un color usa blanco.")]
        public BarColor[] barColors;

        private Image[] segments;
        private Color currentColor = Color.white;


        [System.Serializable]
        public class BarColor
        {
            [Range(0, 1)]
            public float from = 0;
            [Range(0, 1)]
            public float to = 1;
            [ColorUsage(true)]
            public Color color = Color.white;
        }

        public int maxEnergy
        {
            private set; get;
        }

        private int _currentEnergy;
        public int currentEnergy
        {
            set
            {
                if (_currentEnergy != value) UpdateEnergyBar(value);
            }
            get
            {
                return _currentEnergy;
            }
        }

        public float energyPercentage
        {
            get { return ((float)currentEnergy) / ((float)maxEnergy); }
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
           
        }

        public void Initialize(int maxEnergy)
        {
            this.maxEnergy = maxEnergy;
            this._currentEnergy = maxEnergy;
            totalEnergyLabel.text = maxEnergy + "";
            currentEnergyLabel.text = maxEnergy + "";
            CreateSegments();
            SetColor(FindColor(1));
        }

        private void CreateSegments()
        {
            segments = new Image[maxEnergy];
            segments[0] = segment;
            for (int i = 1; i < maxEnergy; i++)
            {
                segments[i] = Instantiate(segment.gameObject, segment.transform.parent).GetComponent<Image>();
            }

            float angle = 360 / maxEnergy;
            for (int i = 0; i < maxEnergy; i++)
            {
                Image segment = segments[i];

                segment.fillAmount = (angle - 5) / 360;
                segment.transform.localRotation = Quaternion.Euler(0, 0, -angle * i);
            }
        }


        private void UpdateEnergyBar(int value)
        {
            _currentEnergy = value;
            for (int i = 0; i < maxEnergy; i++)
            {
                Image segment = segments[i];
                if (segment.gameObject.activeSelf && i >= currentEnergy)
                {
                    StartCoroutine(ClearSegment(segment));
                }
            }
            currentEnergyLabel.text = value + "";
            StartCoroutine(AnimateToColor(FindColor(energyPercentage)));
        }

        private IEnumerator ClearSegment(Image segment)
        {
            float t = 0;
            float time = .25f;
            float startFill = segment.fillAmount;
            while (t < time)
            {
                t += Time.deltaTime;
                segment.fillAmount = Mathf.Lerp(startFill, 0, t / time);
                yield return null;
            }
            segment.gameObject.SetActive(false);
        }

        private IEnumerator AnimateToColor(Color color)
        {
            float t = 0;
            float time = .25f;
            Color startColor = currentColor;
            while (t < time)
            {
                t += Time.deltaTime;
                SetColor(Color.Lerp(startColor, color, t / time));
                yield return null;
            }
            SetColor(color);
        }

        public void SetColor(Color color)
        {
            currentColor = color;
            foreach (Image i in segments)
            {
                i.color = color;
            }
        }

        private Color FindColor(float percentage)
        {
            if (barColors == null || barColors.Length == 0) return Color.white;
            foreach (BarColor c in barColors)
            {
                if (percentage >= c.from && percentage <= c.to) return c.color;
            }
            return Color.white;

        }
    }
}