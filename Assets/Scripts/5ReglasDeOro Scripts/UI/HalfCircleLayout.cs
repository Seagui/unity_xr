﻿using UnityEngine;

public class HalfCircleLayout : MonoBehaviour
{

    public float radius = 200;
    public float centerAngle = 0;
    public float completeAngle = 120;

    private int currentChildCount = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (currentChildCount != transform.childCount)
        {
            UpdatePositions();
        }
    }

    public void UpdatePositions()
    {
        currentChildCount = transform.childCount;
        float center = centerAngle;
        float step = completeAngle / transform.childCount;
        float angle = centerAngle - completeAngle / 2 + step / 2;

        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            child.localPosition = new Vector3(Mathf.Sin(Mathf.Deg2Rad * angle) * radius, Mathf.Cos(Mathf.Deg2Rad * angle) * radius, 0);
            angle += step;
        }
    }


}
