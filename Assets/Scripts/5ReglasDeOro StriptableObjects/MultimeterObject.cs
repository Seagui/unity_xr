﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MultimeterObject", menuName = "Rules/Multimeter")]
public class MultimeterObject : ScriptableObject
{
    public float tension;
}
