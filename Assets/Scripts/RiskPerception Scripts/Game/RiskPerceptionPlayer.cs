﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

namespace OT.Ternium.XR.Framework
{

    public class RiskPerceptionPlayer : MonoBehaviour
    {
        //public List<UIPositionController> positionControllers;
        private static RiskPerceptionPlayer _instance;
        public static RiskPerceptionPlayer instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<RiskPerceptionPlayer>();
                }
                return _instance;
            }
        }

        //public PointerInteraction[] pointers;

        private void Start()
        {
            SetPointersVisible(false);
            
            /*
            if(positionControllers != null)
            {
                positionControllers.ForEach(pc => pc.gameObject.transform.SetParent(gameObject.transform));
            }
            */
        }

        public void SetPointersEnabled(bool enabled)
        {
            /*
            foreach (PointerInteraction pointer in pointers)
            {
                pointer.gameObject.SetActive(enabled);
            }
            */
        }

        public void SetPointersVisible(bool visible)
        {
            /*
            foreach (PointerInteraction pointer in pointers)
            {
                pointer.visible = visible;
            }
            */
        }

        public void EnableLeftPointer(bool enable)
        {
            //var leftPointer = gameObject.GetComponents<SimulatorPointerController>().FirstOrDefault(spc => spc.pointer.name == "LHandPointer");
            /*
            if(leftPointer)
            {
                leftPointer.enabled = enable;
                leftPointer.pointer.enabled = enable;
                SetPointersVisible(enable);
            }
            */
        }

    }
}