﻿using UnityEngine;

namespace OT.Ternium.XR.Framework
{

    public class RiskBehaviour : MonoBehaviour
    {
        public RiskData risk;

        protected virtual void Start()
        {
            /*
            OnRayEnter.AddListener(RayEnter);
            OnRayExit.AddListener(RayExit);
            OnRayStay.AddListener(RayStay);
            OnInteraction.AddListener(Interaction);
            */
        }


        public void RayEnter(Transform t)
        {
            risk.PointerEnter(gameObject);
        }

        public void RayExit(Transform t)
        {
            risk.PointerExit(gameObject);
        }

        public void RayStay(Transform t)
        {

        }

        public void Interaction(Transform t)
        {
            risk.PointerInteract(gameObject);
        }
    }

}