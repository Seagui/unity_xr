﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    public class MessageZoneBehaviour : MonoBehaviour
    {

        public string Title;
        public string Message;

        private void Start()
        {
            if (GetComponent<MeshRenderer>())
            {
                GetComponent<MeshRenderer>().enabled = false;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.name == "HeadCollider")
            {
                RiskPerceptionUI.instance.messages.ShowInfo(Title, Message);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.name == "HeadCollider")
            {

            }
        }
    }
}
