﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    public class RiskPointerFeedback : MonoBehaviour
    {
        public Transform spherePosition = null;

        public virtual void PlayFeedback(RiskPointerInteraction pointer)
        {

        }
    }
}