﻿using UnityEngine;
using System.Linq;

namespace OT.Ternium.XR.Framework
{
    public class RiskPointerInteraction : MonoBehaviour
    {
        public Animator selectionFeedback;

        /*
        protected override void ObjectInteraction(Transform target)
        {
            //TODO: generalizar esta logica
            // TaskBehaviour task = target.gameObject.GetComponentInChildren<Pole>();
            // if(task == null || !task.enabled)
            TaskBehaviour task = target.gameObject.GetComponentsInChildren<TaskBehaviour>().FirstOrDefault(c => c.enabled);
            if(task && !task.task.Completed)
            {
                if(!CheckInteraction(task))
                {
                    task.Error(LanguageSystem.Instance.GetValue("task.error.interactable"));
                    return;
                }

                // TaskBehaviour taskHand = gameObject.GetComponentInChildren<TaskBehaviour>();
                // if(taskHand)
                // {
                //     taskHand.transform.SetParent(null);
                // }

                task.Click();
            }

            RiskBehaviour riskBehaviour = target.GetComponent<RiskBehaviour>();
            if (riskBehaviour != null && !riskBehaviour.risk.found)
            {
                riskBehaviour.OnInteraction.Invoke(transform);
                OnObjectInteraction.Invoke(target);
                PlayFeedback(target);
            }

            FalsePositiveBehaviour falsePositive = target.GetComponent<FalsePositiveBehaviour>();
            if (falsePositive != null)
            {
                falsePositive.OnInteraction.Invoke(transform);
            }

        }
        */

        private bool CheckInteraction(TaskBehaviour task)
        {
            TaskInteractableBehaviour interactable = task as TaskInteractableBehaviour;
            TaskBehaviour taskHand = gameObject.GetComponentInChildren<TaskBehaviour>();
            
            if(interactable == null || taskHand == null)
                return true;
            
            return interactable.CanInteractWith(taskHand);
        }

        private void PlayFeedback(Transform target)
        {
            if (selectionFeedback == null) return;
            RiskPointerFeedback targetFeedback = target.GetComponent<RiskPointerFeedback>();
            if (targetFeedback != null && targetFeedback.spherePosition != null)
            {
                selectionFeedback.transform.position = targetFeedback.spherePosition.position;
            }
            else
            {
                //selectionFeedback.transform.position = dot.transform.position;
            }
            selectionFeedback.SetTrigger("Play");

            if(targetFeedback != null)
            {
                targetFeedback.PlayFeedback(this);
            }
        }

    }
}
