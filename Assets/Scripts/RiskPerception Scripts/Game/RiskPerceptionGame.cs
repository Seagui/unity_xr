﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace OT.Ternium.XR.Framework
{
    public class RiskPerceptionGame : MonoBehaviour
    {

        private static RiskPerceptionGame _instance;
        public static RiskPerceptionGame instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<RiskPerceptionGame>();
                }
                return _instance;
            }
        }

        public float timeLimit = 120;
        public RiskData[] risks;

        public bool debug = false;

        public RiskPerceptionPlayer player
        {
            get { return RiskPerceptionPlayer.instance; }
        }

        public RiskPerceptionUI ui
        {
            get { return RiskPerceptionUI.instance; }
        }

        public bool isGameOver
        {
            get; private set;
        }

        public float time
        {
            get; private set;
        }

        public bool timeIsOver
        {
            get { return time >= timeLimit; }
        }

        public UnityEvent OnGameStart;
        public UnityEvent OnGameOver;
        public UnityEvent OnEnergyLoss;

        public int risksFound
        {
            get; private set;
        }

        public int risksFailed
        {
            get; private set;
        }
        public int risksZoneEntered
        {
            get; private set;
        }
        

        public int risksCount
        {
            get { return risks.Length; }
        }

        public int energy
        {
            get { return risksCount - risksFailed; }
        }


        // Start is called before the first frame update
        void Start()
        {
            foreach (RiskData risk in risks)
            {
                risk.Reset();
                risk.OnDangerZoneEnter += DangerZoneEnter;
                risk.OnDangerZoneExit += DangerZoneExit;
                risk.OnPointerEnter += PointerEnter;
                risk.OnPointerExit += PointerExit;
                risk.OnPointerInteract += PointerInteract;
                risk.OnQuestionAnswer += QuestionAnswered;
            }

            StartGame();
        }

        public void StartGame()
        {
            risksFound = 0;
            risksFailed = 0;
            time = 0;
            OnGameStart.Invoke();
        }

        // Update is called once per frame
        void Update()
        {
            player.SetPointersEnabled(ui.showPointers);
            if (!timeIsOver)
            {
                time += Time.deltaTime;
                if (timeIsOver)
                {
                    time = timeLimit;
                    isGameOver = true;
                    OnGameOver.Invoke();
                }
            }
            else
            {
                time += Time.deltaTime;
            }

        }

        public int GetRiskIndex(RiskData risk)
        {
            for (int i = 0; i < risks.Length; i++)
            {
                if (risks[i].Equals(risk)) return i;
            }
            return 0;
        }


        public void DangerZoneEnter(RiskData risk, GameObject sender)
        {
            if(debug)print("DangerZoneEnter " + risk.title + " from " + sender.name);
            CheckEndGame();
        }

        public void DangerZoneExit(RiskData risk, GameObject sender)
        {
            if (debug) print("DangerZoneExit " + risk.title + " from " + sender.name);
        }

        public void PointerEnter(RiskData risk, GameObject sender)
        {
            if (debug) print("PointerEnter " + risk.title + " from " + sender.name);
        }

        public void PointerExit(RiskData risk, GameObject sender)
        {
            if (debug) print("PointerExit " + risk.title + " from " + sender.name);
        }

        public void PointerInteract(RiskData risk, GameObject sender)
        {
            if (debug) print("PointerInteract " + risk.title + " from " + sender.name);
            ui.ShowQuestion(risk);
        }

        public void QuestionAnswered(RiskData risk, bool isCorrect)
        {
            if (debug) print("QuestionAnswered " + risk.title + " isCorrect=" + isCorrect);

            if (isCorrect)
                StartCoroutine(QuestionCorrectCoroutine(risk));

            UpdateRisksFoundAndFailed();
        }

        public IEnumerator QuestionCorrectCoroutine(RiskData risk)
        {
            yield return new WaitForSeconds(.75f);
            ui.ShowRisksMenu(risk);
            yield return null;
            CheckEndGame();
        }



        public void ShowFalsePositiveMessage(string title, string message)
        {
            ui.messages.ShowWarning(title, message);
        }


        public void CheckEndGame()
        {
            UpdateRisksFoundAndFailed();

            bool allFound = risksFound >= risksCount;
            bool allZones = risksZoneEntered >= risksCount;

            if (allFound || allZones || timeIsOver)
            {
                isGameOver = true;
                OnGameOver.Invoke();
            }

        }

        public void UpdateRisksFoundAndFailed()
        {
            int currentEnergy = energy;
            int found = 0;
            int failed = 0;
            int zones = 0;
            foreach (RiskData risk in risks)
            {
                if (risk.found) found += 1;
                if (risk.dangerZoneEntered || risk.questionFailed) failed += 1;
                if (risk.dangerZoneEntered) zones += 1;
            }
            risksFound = found;
            risksFailed = failed;
            risksZoneEntered = zones;

            if (energy < currentEnergy)
            {
                OnEnergyLoss.Invoke();
            }

        }
    }
}
