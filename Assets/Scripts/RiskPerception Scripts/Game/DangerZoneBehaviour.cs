﻿using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    public class DangerZoneBehaviour : MonoBehaviour
    {
        public RiskData risk;
        public bool energyLoss = true;

        private void Start()
        {
            if (GetComponent<MeshRenderer>())
            {
                GetComponent<MeshRenderer>().enabled = false;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.name == "HeadCollider")
            {
                risk.DangerZoneEnter(gameObject, energyLoss);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.name == "HeadCollider")
            {
                risk.DangerZoneExit(gameObject);
            }
        }

    }
}
