﻿using UnityEngine;

namespace OT.Ternium.XR.Framework
{

    public class FalsePositiveBehaviour : MonoBehaviour
    {
        [TextArea]
        public string title;
        [TextArea]
        public string message;

        protected virtual void Start()
        {
            //OnInteraction.AddListener(Ineraction);
        }

        public void Ineraction(Transform t)
        {
            RiskPerceptionGame.instance.ShowFalsePositiveMessage(title, message);
        }
    }

}