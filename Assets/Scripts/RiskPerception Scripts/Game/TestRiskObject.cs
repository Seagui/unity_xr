﻿using UnityEngine;
using UnityEngine.UI;


namespace OT.Ternium.XR.Framework
{
    [RequireComponent(typeof(RiskBehaviour))]
    public class TestRiskObject : MonoBehaviour
    {

        public Text title;
        public Image image;
        public Image icon;
        public Image color;
        public Image error;

        private RiskBehaviour riskBehaviour;


        void Awake()
        {
            riskBehaviour = GetComponent<RiskBehaviour>();
        }

        private void Start()
        {
            RiskData risk = riskBehaviour.risk;
            image.sprite = risk.image;
            title.text = risk.title;
            icon.sprite = risk.icon;
            color.color = risk.iconSelectedColor;
            error.enabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (riskBehaviour.risk.found) icon.color = color.color;
            error.enabled = riskBehaviour.risk.failed;
        }
    }
}