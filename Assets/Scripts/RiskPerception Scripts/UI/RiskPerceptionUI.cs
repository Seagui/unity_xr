﻿using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    public class RiskPerceptionUI : MonoBehaviour
    {

        private static RiskPerceptionUI _instance;
        public static RiskPerceptionUI instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<RiskPerceptionUI>();
                }
                return _instance;
            }
        }
        [SerializeField]
        private RisksMenu risksMenu;
        [SerializeField]
        private QuestionsUI questionsUI;
        [SerializeField]
        private MessagesUI messagesUI;
        [SerializeField]
        private DangerZonesUI dangerZonesUI;
        //[SerializeField]
        //private SteamVR_Action_Boolean menuBtn;
        [SerializeField]
        private KeyCode menuKey = KeyCode.Escape;



        public MessagesUI messages
        {
            get { return messagesUI; }
        }


        public bool showPointers
        {
            get { return !questionsUI.isVisible && !risksMenu.isVisible && !messages.isVisible; }
        }

        private void Start()
        {
            questionsUI.gameObject.SetActive(false);
            risksMenu.gameObject.SetActive(false);
            messagesUI.gameObject.SetActive(false);
        }

        private void Update()
        {
            if (/*menuBtn.stateDown ||*/ (menuKey != KeyCode.None && Input.GetKeyDown(menuKey)))
            {
                if (!questionsUI.isVisible && !RiskPerceptionGame.instance.isGameOver)
                {
                    if (risksMenu.isVisible) risksMenu.Hide();
                    else ShowRisksMenu();
                }
            }

            if (RiskPerceptionGame.instance.isGameOver)
            {
                if (!risksMenu.isVisible && !questionsUI.isVisible)
                {
                    ShowEndGame();
                }
            }
        }

        private void LateUpdate()
        {
            if (risksMenu.isVisible || messagesUI.isVisible || questionsUI.isVisible)
            {
                //dangerZonesUI.GetComponent<UIPositionController>().enabled = false;
                dangerZonesUI.transform.position = transform.position;
                dangerZonesUI.transform.localEulerAngles = transform.localEulerAngles + new Vector3(0, 60, 0);
            }
            else
            {
                //dangerZonesUI.GetComponent<UIPositionController>().enabled = true;
            }
        }

        public void ShowQuestion(RiskData risk)
        {
            if (showPointers) Recenter();
            risksMenu.Hide();
            questionsUI.Show(risk);
        }

        public void ShowRisksMenu(RiskData risk = null)
        {
            if (showPointers) Recenter();
            questionsUI.Hide();
            risksMenu.Show(risk);
        }

        public void ShowEndGame()
        {
            if (showPointers) Recenter();
            risksMenu.ShowEndGame();
        }


        public void Recenter()
        {
            //GetComponent<UIPositionController>().Recenter();
        }
    }
}
