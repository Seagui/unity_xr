﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace OT.Ternium.XR.Framework
{
    [RequireComponent(typeof(Image))]
    public class RiskMenuIcon : MonoBehaviour
    {
        private RiskData _risk;
        public RiskData risk
        {
            get
            {
                return _risk;
            }
            set
            {
                _risk = value;
                icon.sprite = risk.icon;
            }
        }

        [SerializeField]
        private Image icon;
        [SerializeField]
        private Image borderSelected;
        [SerializeField]
        private Image borderUnselected;
        [SerializeField]
        private Image backSelected;
        [SerializeField]
        private Image backUnselected;



        public UnityEvent onClick
        {
            get { return GetComponent<Button>().onClick; }
        }

        [HideInInspector]
        public bool isSelected = false;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            bool enabled = risk != null && risk.found;
            Color color = (risk != null) ? risk.iconSelectedColor : Color.white;

            GetComponent<Button>().interactable = enabled;

            borderSelected.gameObject.SetActive(isSelected);
            borderUnselected.gameObject.SetActive(!isSelected);

            backSelected.gameObject.SetActive(isSelected);
            backUnselected.gameObject.SetActive(!isSelected);

            backSelected.color = color;
            backUnselected.color = color;
            borderSelected.color = color;

            borderUnselected.color = icon.color = enabled ? Color.white : new Color(1, 1, 1, .5f);

            backUnselected.gameObject.SetActive(enabled);

        }
    }
}