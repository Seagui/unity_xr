﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    [RequireComponent(typeof(StatusUI))]
    [RequireComponent(typeof(CanvasGroupAnimation))]
    public class EnergyLossMessage : MonoBehaviour
    {
        private StatusUI statusUI;
        private CanvasGroupAnimation canvasGroupAnimation;

        // Start is called before the first frame update
        void Start()
        {
            statusUI = GetComponent<StatusUI>();
            canvasGroupAnimation = GetComponent<CanvasGroupAnimation>();
            RiskPerceptionGame.instance.OnEnergyLoss.AddListener(EnergyLoss);
            canvasGroupAnimation.Hide(0);
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void EnergyLoss()
        {
            print(name + " EnergyLoss");
            StartCoroutine(Show());
        }

        private IEnumerator Show()
        {
            statusUI.enabled = false;
            canvasGroupAnimation.Show(.35f);
            yield return new WaitForSeconds(.35f);
            statusUI.enabled = true;
            yield return new WaitForSeconds(2);
            statusUI.enabled = false;
            canvasGroupAnimation.Hide(.35f);
            yield return null;
        }




    }

}
