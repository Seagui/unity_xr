﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace OT.Ternium.XR.Framework
{

    [RequireComponent(typeof(CanvasGroupAnimation))]
    public class RisksMenu : MonoBehaviour
    {
        public RiskDetailsPanel details;
        public GameObject detailsPlaceholder;
        public Transform listParent;
        public GameObject iconPrefab;
        public GameObject endGameWin;
        public GameObject endGameLose;
        public GameObject endGameTime;
        public GameObject riskStatusIconPrefab;
        public Transform riskStatusIconsParent;
        public Button closeBtn;



        private List<RiskMenuIcon> riskIcons = new List<RiskMenuIcon>();

        private CanvasGroupAnimation _animation;
        private new CanvasGroupAnimation animation
        {
            get
            {
                if (_animation == null) _animation = GetComponent<CanvasGroupAnimation>();
                return _animation;
            }
        }

        public bool isVisible
        {
            get { return animation.isVisible; }
        }

        private bool initialized = false;

        private RiskPerceptionGame game
        {
            get { return RiskPerceptionGame.instance; }
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Initialize()
        {
            initialized = true;
            animation.Hide(0);
            RiskData[] risks = game.risks;
            details.gameObject.SetActive(true);
            ClearIcons();
            foreach (RiskData r in risks)
            {
                RiskData risk = r;
                RiskMenuIcon icon = Instantiate(iconPrefab, listParent).GetComponent<RiskMenuIcon>();
                icon.risk = risk;
                icon.onClick.AddListener(delegate
                {
                    Show(risk);
                });
                riskIcons.Add(icon);
            }
            endGameWin.SetActive(false);
            endGameLose.SetActive(false);
            endGameTime.SetActive(false);
            endGameWin.transform.localScale = new Vector3(0, 1, 1);
            endGameLose.transform.localScale = new Vector3(0, 1, 1);
            endGameTime.transform.localScale = new Vector3(0, 1, 1);
            closeBtn.interactable = true;
            listParent.gameObject.SetActive(true);
            listParent.localScale = new Vector3(1, 1, 1);
        }

        private void ClearIcons()
        {
            List<GameObject> destroy = new List<GameObject>();
            foreach (Transform t in listParent)
                destroy.Add(t.gameObject);
            foreach (GameObject go in destroy)
                Destroy(go);
        }

        public void Show(RiskData risk = null)
        {
            if (!initialized) Initialize();
            animation.Show();
            if (risk == null)
            {
                foreach(RiskData r in game.risks)
                {
                    if (r.found) risk = r;
                }
            }

            if(risk != null)
            {
                detailsPlaceholder.SetActive(false); 
                int index = game.GetRiskIndex(risk);
                SetIconSelected(index);
                details.ShowDetails(risk);
            }
            else
            {
                SetIconSelected(-1);
                details.Hide(0);
                detailsPlaceholder.SetActive(true);
            }

        }

        public void Hide()
        {
            if (!initialized) Initialize();
            else
            {
                if (details.isVisible) details.Hide();

                if (game.isGameOver)
                {
                    ShowEndGame();
                }
                else
                {
                    animation.Hide();
                }
            }
        }

        private void SetIconSelected(int index)
        {
            for (int i = 0; i < riskIcons.Count; i++)
            {
                riskIcons[i].isSelected = i == index;
            }
        }

        public void ShowEndGame()
        {
            if (!initialized) Initialize();
            animation.Show();

            detailsPlaceholder.SetActive(false);
            closeBtn.interactable = false;

            if (game.energy <= 0)
            {
                StartCoroutine(ShowEndGamePanel(endGameLose));
            }
            else if (game.risksFound >= game.risksCount)
            {
                StartCoroutine(ShowEndGamePanel(endGameWin));
            }
            else if (game.timeIsOver)
            {
                StartCoroutine(ShowEndGamePanel(endGameTime));
                CreateFinalPanelIcons(RiskPerceptionGame.instance.risks);
            }

            SetIconSelected(-1);
            CanvasGroup listCanvasGroup = listParent.GetComponent<CanvasGroup>();
            if (listCanvasGroup == null) listCanvasGroup = listParent.gameObject.AddComponent<CanvasGroup>();
            listCanvasGroup.interactable = listCanvasGroup.blocksRaycasts = false;
        }

        private IEnumerator ShowEndGamePanel(GameObject panel)
        {
            if (details.isVisible)
            {
                details.Hide();
                yield return new WaitForSeconds(1f);
            }
            else
            {
                details.gameObject.SetActive(false);
            }
            float t = 0;
            panel.SetActive(true);
            panel.transform.localScale = new Vector3(0, 1, 1);
            while (t < .25f)
            {
                t += Time.deltaTime;
                panel.transform.localScale = new Vector3(t / .25f, 1, 1);
                listParent.transform.localScale = new Vector3(1 - t / .25f, 1, 1);
                yield return null;
            }
            panel.transform.localScale = new Vector3(1, 1, 1);
            listParent.gameObject.SetActive(false);
        }

        private void ClearFinalPanelIcons()
        {
            List<GameObject> destroy = new List<GameObject>();
            foreach (Transform t in riskStatusIconsParent)
                destroy.Add(t.gameObject);
            foreach (GameObject go in destroy)
                Destroy(go);
        }

        public void CreateFinalPanelIcons(RiskData[] risks)
        {
            ClearFinalPanelIcons();
            foreach (RiskData risk in risks)
            {
                if (!risk.found)
                {
                    RiskStatusIcon icon = Instantiate(riskStatusIconPrefab, riskStatusIconsParent).GetComponent<RiskStatusIcon>();
                    icon.risk = risk;
                }
            }
        }
    }

}