﻿using UnityEngine;
using UnityEngine.UI;

namespace OT.Ternium.XR.Framework
{
    public class StepStatusIcon : MonoBehaviour
    {
        private StepObject _step;

        private Animator animator;
        public StepObject step
        {
            get
            {
                return _step;
            }
            set
            {
                _step = value;
                iconImage.sprite = step.icon;
            }
        }

        public Image iconImage;
        public Image statusImage;

        public Color statusDefault = Color.black;
        public Color statusDone = Color.green;
        public Color statusFailed = Color.red;
        public bool statusDoneUsesRiskColor = true;
        public float defaultAlpha = .75f;
        public float foundAlpha = 1;

        private CanvasGroup cGroup;


        // Start is called before the first frame update
        void Start()
        {
            animator = GetComponent<Animator>();
            cGroup = GetComponent<CanvasGroup>();
        }

        // Update is called once per frame
        void Update()
        {
            if (step != null && step.Completed)
            {
                statusImage.color = statusDone;
            }
            else 
            {
                statusImage.color = statusDefault;
            }

            // if (animator) animator.SetBool("failed", step != null && step.failed);

            if (cGroup != null)
                cGroup.alpha = step != null && step.Completed ? foundAlpha : defaultAlpha;

        }

    }
}