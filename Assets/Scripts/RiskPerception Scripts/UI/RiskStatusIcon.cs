﻿using UnityEngine;
using UnityEngine.UI;

namespace OT.Ternium.XR.Framework
{
    public class RiskStatusIcon : MonoBehaviour
    {
        private RiskData _risk;

        private Animator animator;
        public RiskData risk
        {
            get
            {
                return _risk;
            }
            set
            {
                _risk = value;
                iconImage.sprite = risk.icon;
            }
        }

        public Image iconImage;
        public Image statusImage;

        public Color statusDefault = Color.black;
        public Color statusDone = Color.green;
        public Color statusFailed = Color.red;
        public bool statusDoneUsesRiskColor = true;
        public float defaultAlpha = .75f;
        public float foundAlpha = 1;

        private CanvasGroup cGroup;


        // Start is called before the first frame update
        void Start()
        {
            animator = GetComponent<Animator>();
            cGroup = GetComponent<CanvasGroup>();
        }

        // Update is called once per frame
        void Update()
        {
            if (risk != null && risk.found && !risk.failed)
            {
                if (statusDoneUsesRiskColor) statusImage.color = risk.iconSelectedColor;
                else statusImage.color = statusDone;
            }
            else if (risk != null && risk.failed) statusImage.color = statusFailed;
            else statusImage.color = statusDefault;

            if (animator) animator.SetBool("failed", risk != null && risk.failed);

            if (cGroup != null)
                cGroup.alpha = risk != null && risk.found ? foundAlpha : defaultAlpha;

        }

    }
}