﻿using System.Collections.Generic;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{

    public class StatusUI : MonoBehaviour
    {

        public GameObject riskStatusIconPrefab;
        public Transform risksIconsParent;
        public EnergyBar energyBar;
        public TimerUI timer;
        public RuleObject rule;

        private List<StepStatusIcon> icons = new List<StepStatusIcon>();

        // Start is called before the first frame update
        void Start()
        {
            Initialize(GetStepsWithStatus());
        }

        List<StepObject> GetStepsWithStatus()
        {
            List<StepObject> steps = new List<StepObject>();

            foreach (StepObject step in rule.steps)
                if (step.showInStatus)
                    steps.Add(step);

            return steps;
        }

        // Update is called once per frame
        void Update()
        {
            timer.time += Time.deltaTime;

            // energyBar.currentEnergy = RiskPerceptionGame.instance.energy;
        }

        public void Initialize(List<StepObject> steps)
        {
            ClearIcons();
            foreach (StepObject step in steps)
            {
                StepStatusIcon icon = Instantiate(riskStatusIconPrefab, risksIconsParent).GetComponent<StepStatusIcon>();
                icon.step = step;
                icons.Add(icon);
            }
            energyBar.Initialize(steps.Count);
        }

        private void ClearIcons()
        {
            List<GameObject> destroy = new List<GameObject>();
            foreach (Transform t in risksIconsParent)
                destroy.Add(t.gameObject);
            foreach (GameObject go in destroy)
                Destroy(go);
        }

        private void OnEnable()
        {
            foreach(StepStatusIcon icon in icons)
            {
                icon.enabled = true;
            }
            energyBar.enabled = true;
            timer.enabled = true;
        }

        private void OnDisable()
        {
            foreach (StepStatusIcon icon in icons)
            {
                icon.enabled = false;
            }
            energyBar.enabled = false;
            timer.enabled = false;
        }


    }
}