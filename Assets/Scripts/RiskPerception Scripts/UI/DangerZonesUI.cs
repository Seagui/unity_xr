﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace OT.Ternium.XR.Framework
{
    public class DangerZonesUI : MonoBehaviour
    {
        public GameObject messagePrefab;
        public Transform messagesParent;

        private Dictionary<string, DangerZoneMessage> messages = new Dictionary<string, DangerZoneMessage>();
        private Dictionary<string, int> zonesCount = new Dictionary<string, int>();


        private void Start()
        {
            foreach (RiskData risk in RiskPerceptionGame.instance.risks)
            {
                risk.OnDangerZoneEnter += DangerZoneEnter;
                risk.OnDangerZoneExit += DangerZoneExit;
            }


            List<GameObject> destroy = new List<GameObject>();
            foreach (Transform t in messagesParent)
                destroy.Add(t.gameObject);
            foreach (GameObject go in destroy)
                Destroy(go);
        }


        public void DangerZoneEnter(RiskData risk, GameObject sender)
        {
            if (!zonesCount.ContainsKey(risk.name)) zonesCount.Add(risk.name, 0);
            zonesCount[risk.name] += 1;

            if (!messages.ContainsKey(risk.name)) messages.Add(risk.name, CreateMessage(risk));


            GameObject zoneMessage = messages[risk.name].gameObject;
            zoneMessage.SetActive(true);


            int count = 0;
            foreach(Transform t in messagesParent)
            {
                if (t.gameObject.activeSelf) count += 1;
            }

            if (zoneMessage.GetComponent<Animator>())
            {
                zoneMessage.transform.SetAsLastSibling();
                zoneMessage.GetComponent<Animator>().SetBool("first", count == 1);
                zoneMessage.GetComponent<Animator>().SetBool("visible", true);
            }

        }

        public void DangerZoneExit(RiskData risk, GameObject sender)
        {
            zonesCount[risk.name] -= 1;
            if (zonesCount[risk.name] < 0) zonesCount[risk.name] = 0;
            if (zonesCount[risk.name] == 0)
            {
                StartCoroutine(HideZone(messages[risk.name].gameObject));
            }
        }

        private IEnumerator HideZone(GameObject zoneMessage)
        {
            if (zoneMessage.GetComponent<Animator>())
            {
                zoneMessage.GetComponent<Animator>().SetBool("visible", false);
                yield return new WaitForSeconds(.35f);
                if (!zoneMessage.GetComponent<Animator>().GetBool("visible"))
                {
                    zoneMessage.SetActive(false);
                    zoneMessage.transform.SetAsLastSibling();
                }
            }
            else
            {
                zoneMessage.SetActive(false);
                zoneMessage.transform.SetAsLastSibling();
            }
        }


        private DangerZoneMessage CreateMessage(RiskData risk)
        {
            DangerZoneMessage message = Instantiate(messagePrefab, messagesParent).GetComponent<DangerZoneMessage>();
            message.risk = risk;
            return message;
        }
    }
}