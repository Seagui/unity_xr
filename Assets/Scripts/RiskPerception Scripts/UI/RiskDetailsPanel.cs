﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace OT.Ternium.XR.Framework
{
    public class RiskDetailsPanel : MonoBehaviour
    {

        public Text title;
        public Image image;
        public Transform infographicsParent;
        public Transform infographicsParent4Plus;
        public GameObject infographicsPrefab;
        public Text infoTitle;
        public Text infoDescription;
        public Text ruleNumber;
        public Text ruleTitle;
        public Text ruleDescription;
        public Image ruleIcon;
        public Animator ruleDetailsAnimator;

        private Coroutine currentAnimation;
        private RiskData currentRisk = null;

        private bool initialized = false;

        public bool isVisible
        {
            get; private set;
        }


        // Start is called before the first frame update
        void Awake()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ShowDetails(RiskData risk)
        {
            ShowLifeSavingRules(risk);

            if (!initialized)
            {
                initialized = true;
                transform.localScale = new Vector3(0, 1, 1);
                isVisible = false;
            }

            if (currentRisk == risk)
            {
                if (!isVisible)
                {
                    StopAllCoroutines();
                    transform.localScale = new Vector3(1, 1, 1);
                    isVisible = true;
                }
                return;
            }
            else currentRisk = risk;

            if (risk == null)
            {
                transform.localScale = new Vector3(0, 1, 1);
                isVisible = false;
            }
            else
            {
                isVisible = true;
                StopAllCoroutines();
                StartCoroutine(ShowDetailsCoroutine(risk));
            }

        }

        private void ShowLifeSavingRules(RiskData risk)
        {
            bool show = risk.lifeSavingRules.Length > 0;
            this.ruleNumber.gameObject.SetActive(show);
            this.ruleTitle.gameObject.SetActive(show);
            this.ruleDescription.gameObject.SetActive(show);
            this.ruleDetailsAnimator.gameObject.SetActive(show);
        }

        private void CreateInfographics(Sprite[] infographics)
        {
            List<GameObject> destroy = new List<GameObject>();
            foreach (Transform t in infographicsParent)
                destroy.Add(t.gameObject);
            foreach (Transform t in infographicsParent4Plus)
                destroy.Add(t.gameObject);

            foreach (GameObject go in destroy)
                Destroy(go);

            Transform list = infographicsParent;
            if (infographics.Length > 3)
            {
                list = infographicsParent4Plus;
            }

            foreach (Sprite s in infographics)
            {
                Image icon = Instantiate(infographicsPrefab, list).GetComponent<Image>();
                icon.sprite = s;
            }
        }





        public IEnumerator ShowDetailsCoroutine(RiskData risk)
        {
            float time = .35f;
            AnimationCurve curve = AnimationCurve.EaseInOut(0, 0, 1, 1);
            float t = 0;
            Vector3 startScale = transform.localScale;
            if (!startScale.Equals(new Vector3(0, 1, 1)))
            {
                yield return StartCoroutine(HideCoroutine(time));
            }
            SetData(risk);
            t = 0;
            startScale = transform.localScale;
            while (t < time)
            {
                transform.localScale = Vector3.Lerp(startScale, new Vector3(1, 1, 1), curve.Evaluate(t / time));
                t += Time.deltaTime;
                yield return null;
            }
            yield return null;
        }

        private void SetData(RiskData risk)
        {
            title.text = risk.title;
            image.sprite = risk.image;
            infoTitle.text = risk.infoTitle;
            infoDescription.text = risk.description;
            ruleNumber.text = risk.lifeSavingRules.Length > 0 ? risk.lifeSavingRules[0].number.ToString() : "0";
            ruleTitle.text = risk.lifeSavingRules.Length > 0 ? risk.lifeSavingRules[0].title : string.Empty;
            ruleDescription.text = risk.lifeSavingRules.Length > 0 ? risk.lifeSavingRules[0].description : string.Empty;
            ruleIcon.sprite = risk.lifeSavingRules.Length > 0  ? risk.lifeSavingRules[0].icon : null;
            CreateInfographics(risk.infographics);
        }

        public void Hide(float time = .25f)
        {
            if(time <= 0)
            {
                isVisible = false;
                transform.localScale = new Vector3(0, 1, 1);
                return;
            }
            isVisible = false;
            this.ruleNumber.gameObject.SetActive(true);
            this.ruleDescription.gameObject.SetActive(true);
            this.ruleDetailsAnimator.gameObject.SetActive(true);
            StopAllCoroutines();
            StartCoroutine(HideCoroutine(time));
        }


        private IEnumerator HideCoroutine(float time)
        {
            AnimationCurve curve = AnimationCurve.EaseInOut(0, 0, 1, 1);
            float t = 0;
            Vector3 startScale = transform.localScale;
            if (!startScale.Equals(new Vector3(0, 1, 1)))
            {
                while (t < time)
                {
                    transform.localScale = Vector3.Lerp(startScale, new Vector3(0, 1, 1), curve.Evaluate(t / time));
                    t += Time.deltaTime;
                    yield return null;
                }
                transform.localScale = new Vector3(0, 1, 1);
            }
            yield return null;
        }
    }
}