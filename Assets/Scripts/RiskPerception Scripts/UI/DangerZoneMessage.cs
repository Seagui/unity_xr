﻿using UnityEngine;
using UnityEngine.UI;

namespace OT.Ternium.XR.Framework
{
    public class DangerZoneMessage : MonoBehaviour
    {
        public Image icon;
        public Text title;

        private RiskData _risk;
        public RiskData risk
        {
            get { return _risk; }
            set
            {
                _risk = value;
                icon.sprite = _risk.icon;
                title.text = _risk.title;
            }
        }

    }
}