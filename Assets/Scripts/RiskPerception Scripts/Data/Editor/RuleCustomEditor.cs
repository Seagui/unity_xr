﻿using UnityEditor;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{

    [CustomEditor(typeof(RuleData))]
    public class RuleCustomEditor : Editor
    {
        RuleData comp;

        public void OnEnable()
        {
            comp = (RuleData) target;
        }

        public override void OnInspectorGUI()
        {
            EditorStyles.textArea.wordWrap = true;
            EditorStyles.textArea.stretchHeight = true;

            EditorGUILayout.Space();
            Multilanguage.current = (Language)EditorGUILayout.EnumPopup(new GUIContent("Language: "), Multilanguage.current);
            EditorGUILayout.Space();

            comp.number = EditorGUILayout.IntField("Rule Number", comp.number);

            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Title");
            comp.title = EditorGUILayout.TextArea(comp.title, EditorStyles.textArea, GUILayout.ExpandHeight(true), GUILayout.MinHeight(64f));

            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Description");
            comp.description = EditorGUILayout.TextArea(comp.description, EditorStyles.textArea, GUILayout.ExpandHeight(true), GUILayout.MinHeight(128f));

            EditorGUILayout.Space();


            EditorGUILayout.LabelField("Icon", GUILayout.Width(65f));
            comp.icon = (Sprite)EditorGUILayout.ObjectField(comp.icon, typeof(Sprite), false, GUILayout.Width(65f), GUILayout.Height(65f));

            EditorUtility.SetDirty(comp);

        }

    }
}