﻿using UnityEditor;
using UnityEngine;


namespace OT.Ternium.XR.Framework
{

    [CustomEditor(typeof(RiskData))]
    public class RiskCustomEditor : Editor
    {
        RiskData comp;
        bool showDefaultEditor = false;

        public void OnEnable()
        {
            comp = (RiskData)target;
        }

        public override void OnInspectorGUI()
        {

            EditorStyles.textArea.wordWrap = true;
            EditorStyles.textArea.stretchHeight = true;

            EditorGUILayout.Space();
            Multilanguage.current = (Language)EditorGUILayout.EnumPopup(new GUIContent("Language: "), Multilanguage.current);
            EditorGUILayout.Space();

            GUILayout.Label("Title");
            comp.title = GUILayout.TextField(comp.title);

            EditorGUILayout.Space();

            GUILayout.Label("Info Title");
            comp.infoTitle = GUILayout.TextField(comp.infoTitle);

            EditorGUILayout.Space();

            GUILayout.Label("Description");
            comp.description = GUILayout.TextArea(comp.description, GUILayout.Height(64));

            EditorGUILayout.Space();

            comp.iconSelectedColor = EditorGUILayout.ColorField(new GUIContent("Color"), comp.iconSelectedColor, false, false, false);

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Icon", GUILayout.Width(128));
            GUILayout.Label("Image", GUILayout.Width(218));
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            comp.icon = (Sprite)EditorGUILayout.ObjectField(comp.icon, typeof(Sprite), false, GUILayout.Width(128), GUILayout.Height(128));
            comp.image = (Sprite)EditorGUILayout.ObjectField(comp.image, typeof(Sprite), false, GUILayout.Width(218), GUILayout.Height(128));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            int infographicsCount = comp.infographics.Length;
            infographicsCount = EditorGUILayout.IntField(new GUIContent("Infographics Count"), infographicsCount);
            if(infographicsCount != comp.infographics.Length)
            {
                Sprite[] prevList = comp.infographics;
                comp.infographics = new Sprite[infographicsCount];
                for(int i = 0; i < Mathf.Min(prevList.Length, infographicsCount); i++)
                {
                    comp.infographics[i] = prevList[i];
                }
            }
            EditorGUILayout.BeginHorizontal();
            for (int i = 0; i < infographicsCount; i++)
            {
                comp.infographics[i] = (Sprite)EditorGUILayout.ObjectField(comp.infographics[i], typeof(Sprite), false, GUILayout.Width(64), GUILayout.Height(64));
            }
            EditorGUILayout.EndHorizontal();


            EditorGUILayout.Space();
            EditorGUILayout.Space();

            int rulesCount = comp.lifeSavingRules.Length;
            rulesCount = EditorGUILayout.IntField(new GUIContent("Rules Count"), rulesCount);
            if (rulesCount != comp.lifeSavingRules.Length)
            {
                RuleData[] prevList = comp.lifeSavingRules;
                comp.lifeSavingRules = new RuleData[rulesCount];
                for (int i = 0; i < Mathf.Min(prevList.Length, comp.lifeSavingRules.Length); i++)
                {
                    comp.lifeSavingRules[i] = prevList[i];
                }
            }

            for (int i = 0; i < rulesCount; i++)
            {
                comp.lifeSavingRules[i] = (RuleData)EditorGUILayout.ObjectField(comp.lifeSavingRules[i], typeof(RuleData), false);
            }

            EditorGUILayout.Space();


            GUILayout.Label("Question");
            comp.question.text = EditorGUILayout.TextField(comp.question.text);

            EditorGUILayout.Space();

            int answersCount = comp.question.answers.Length;
            answersCount = EditorGUILayout.IntField(new GUIContent("Answers Count"), answersCount);
            if (answersCount != comp.question.answers.Length)
            {
                RiskData.Answer[] prevList = comp.question.answers;
                comp.question.answers = new RiskData.Answer[answersCount];
                for (int i = 0; i < Mathf.Min(prevList.Length, comp.question.answers.Length); i++)
                {
                    comp.question.answers[i] = prevList[i];
                }
            }
            EditorGUILayout.Space();


            if (answersCount > 0)
            {
                EditorGUILayout.BeginHorizontal();
                GUILayout.Label("   Correct", GUILayout.Width(64));
                GUILayout.Label("Text");


                EditorGUILayout.EndHorizontal();
            }

            for (int i = 0; i < answersCount; i++)
            {
                EditorGUILayout.BeginHorizontal();
                GUILayout.Label(i + "", GUILayout.Width(32));
                if (comp.question.answers[i] == null) comp.question.answers[i] = new RiskData.Answer();
                comp.question.answers[i].isCorrect = EditorGUILayout.Toggle(comp.question.answers[i].isCorrect, GUILayout.Width(32));
                comp.question.answers[i].text = EditorGUILayout.TextField(comp.question.answers[i].text);

                EditorGUILayout.EndHorizontal();
            }

            if (showDefaultEditor)
            {
                EditorGUILayout.Space();
                DrawDefaultInspector();
            }

            EditorUtility.SetDirty(comp);
        }
    }
}