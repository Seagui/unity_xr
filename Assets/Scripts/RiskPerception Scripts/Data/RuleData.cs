﻿using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    [CreateAssetMenu(menuName = "Risk Perception/Life Saving Rule")]
    public class RuleData : ScriptableObject
    {
        public int number;
        [SerializeField]
        public MultilanguageString _title;

        public Sprite icon;

        [SerializeField]
        public MultilanguageString _description;

        public string title
        {
            get { return _title.value; }
            set { _title.value = value; }
        }

        public string description
        {
            get { return _description.value; }
            set { _description.value = value; }
        }

    }

}