﻿using System;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{

    [CreateAssetMenu(menuName = "Risk Perception/Risk")]
    public class RiskData : ScriptableObject
    {
        //-----------------//
        // Serialized data //
        //-----------------//
        [SerializeField]
        private MultilanguageString _title;
        
        [SerializeField]
        private MultilanguageString _infoTitle;

        [SerializeField]
        private MultilanguageString _description;

        public Sprite icon;
        public Color iconSelectedColor = new Color(.25f, .5f, 1f);
        public Sprite image;
        public Sprite[] infographics = new Sprite[] { };
        public RuleData[] lifeSavingRules = new RuleData[] { };
        public Question question;


        //------------//  Mantiene compatibilidad con
        // Properties //  el codigo anterior a multiidioma
        //------------//  donde estas eran las variables


        public string title
        {
            get { return _title.value; }
            set { _title.value = value; }
        }

        public string infoTitle
        {
            get { return _infoTitle.value; }
            set { _infoTitle.value = value; }
        }

        public string description
        {
            get { return _description.value; }
            set { _description.value = value; }
        }



        //------------//
        // Game state //
        //------------//
        public bool found
        {
            get; private set;
        }

        public bool failed
        {
            get { return dangerZoneEntered || questionFailed; }
        }

        public bool dangerZoneEntered
        {
            get; private set;
        }


        public bool questionFailed
        {
            get; private set;
        }

        public bool questionAnswered
        {
            get; private set;
        }

        //--------//
        // Events //
        //--------//
        public delegate void RiskInteractionEvent(RiskData risk, GameObject sender);
        public RiskInteractionEvent OnPointerEnter;
        public RiskInteractionEvent OnPointerExit;
        public RiskInteractionEvent OnPointerInteract;
        public RiskInteractionEvent OnDangerZoneEnter;
        public RiskInteractionEvent OnDangerZoneExit;

        public delegate void RiskQuestionEvent(RiskData risk, bool isCorrect);
        public RiskQuestionEvent OnQuestionAnswer;


        //------------------//
        // Public functions //
        //------------------//
        public bool CheckAnswer(int answerIndex)
        {
            Debug.Log("CheckAnswer " + answerIndex + " " + name);
            bool isCorrect = false;
            try
            {
                isCorrect = question.answers[answerIndex].isCorrect;
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
                Debug.Log(e.StackTrace);
            }

            if (!isCorrect)
            {
                questionFailed = true;
            }
            else
            {
                questionAnswered = true;
            }

            OnQuestionAnswer.Invoke(this, isCorrect);

            return isCorrect;
        }

        public void PointerEnter(GameObject sender)
        {
            OnPointerEnter.Invoke(this, sender);
        }

        public void PointerExit(GameObject sender)
        {
            OnPointerExit.Invoke(this, sender);
        }

        public void PointerInteract(GameObject sender)
        {
            if (!found)
            {
                found = true;
                OnPointerInteract.Invoke(this, sender);
            }
        }

        public void DangerZoneEnter(GameObject sender, bool energyLoss = true)
        {
            if (energyLoss)
                dangerZoneEntered = true;

            OnDangerZoneEnter.Invoke(this, sender);
        }

        public void DangerZoneExit(GameObject sender)
        {
            OnDangerZoneExit.Invoke(this, sender);
        }

        public void Reset()
        {
            found = false;
            questionFailed = false;
            dangerZoneEntered = false;
            questionAnswered = false;
        }

        //---------------//
        // Inner classes //
        //---------------//
        [System.Serializable]
        public class Question
        {
            [SerializeField]
            public MultilanguageString _text;

            public Answer[] answers = new Answer[] { };

            public string text
            {
                get { return _text.value; }
                set { _text.value = value; }
            }
        }

        [System.Serializable]
        public class Answer
        {
            [SerializeField]
            private MultilanguageString _text;

            public bool isCorrect = false;

            public string text
            {
                get { return _text.value; }
                set { _text.value = value; }
            }
        }

    }
}