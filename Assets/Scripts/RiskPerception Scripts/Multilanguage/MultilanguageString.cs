﻿
[System.Serializable]
public class MultilanguageString
{
    public string es;
    public string en;
    public string pt;

    public string value
    {
        get {
            switch (Multilanguage.current)
            {
                case Language.ES: return es;
                case Language.EN: return en;
                case Language.PT: return pt;
                default: return es;
            } 
        }
        set
        {
            switch (Multilanguage.current)
            {
                case Language.ES: es = value; break;
                case Language.EN: en = value; break;
                case Language.PT: pt = value; break;
            }
        }
    }
}
