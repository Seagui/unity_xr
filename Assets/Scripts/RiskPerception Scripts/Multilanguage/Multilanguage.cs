﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Multilanguage : MonoBehaviour
{
    private static Language _current = Language.ES;
    public static Language current {
        get { 
            return _current; 
        }
        set
        {
            _current = value;
        }
    }
}
