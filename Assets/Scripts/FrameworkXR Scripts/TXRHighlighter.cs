﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TXRHighlighter : MonoBehaviour
{

    protected Material _currentMaterial;

    [Header("Highlither")]
    [SerializeField] protected Material highlightMaterial;

    protected abstract void ToggleHighlight(bool isOn);

    public void SelectEnter()
    {
        ToggleHighlight(true);
    }

    public void SelectExit()
    {
        ToggleHighlight(false);
    }

    public void ChangeColor(bool red)
    {
        highlightMaterial.SetTexture("Texture2D_722E04C9", _currentMaterial.mainTexture);
        if (red)
            highlightMaterial.SetColor("Color_E35AB720", Color.red);
        else
            highlightMaterial.SetColor("Color_E35AB720", Color.blue);
    }

}