﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using OT.Ternium.XR.Framework;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.SceneManagement;

namespace OT.Ternium.XR.Framework
{

    public class GameLogicTutorial : InteractionOnlyInUI
    {

        [SerializeField]
        private GameObject vrCamera;
        [SerializeField]
        private RuleBehaviour ruleBehaviour;
        [SerializeField]
        private UI_System uiSystem;
        [SerializeField]
        private UI_Screen taskUiScreen;
        [SerializeField]
        private LanguageText taskUiLanguageTextTitle;
        [SerializeField]
        private LanguageText taskUiLanguageTextText;
        [SerializeField]
        private LanguageText taskUiLanguageTextNote;
        [SerializeField]
        private UI_Screen messageScreen;
        [SerializeField]
        private UI_Screen questionScreen;
        [SerializeField]
        private XRRayInteractor leftRayTeleport;
        [SerializeField]
        private XRRayInteractor rightRayTeleport;
        [SerializeField]
        private XRNode xrNode = XRNode.LeftHand;

        [SerializeField]
        private UI_Screen skipTutorialStep;
        [SerializeField]
        private string movementStepKeyTitle;
        [SerializeField]
        private string movementStepKeyText;
        [SerializeField]
        private string movementStepKeyNote;
        [SerializeField]
        [Range(0,1)] private float movementDistance = 1f;
        [SerializeField]
        private string teleportStepKeyTitle;
        [SerializeField]
        private string teleportStepKeyText;
        [SerializeField]
        private string teleportStepKeyNote;
        [SerializeField]
        private List<TaskObjectTutorial> taskSteps;
        [SerializeField]
        private UI_Screen uiTaskScreen;
        [SerializeField]
        private UI_Screen finishTutorialScreen;

        private bool waitingForTeleport = true;
        private List<bool> waitingForTask;

        private InputDevice device;
        Vector3 positionStep0;
        int stepID = 0;
        int taskStepID = 0;

        // Start is called before the first frame update
        void Start()
        {
            waitingForTask = new List<bool>();

            positionStep0 = vrCamera.transform.position;

            uiSystem.CloseAllScreen();

            uiSystem.SwitchScreen(taskUiScreen);

            stepID = 0;

            device = InputDevices.GetDeviceAtXRNode(xrNode);

        }

        // Update is called once per frame
        void Update()
        {

            switch (stepID)
            {
                case 0:
                    bool primaryButtonValue = false;
                    if (device.TryGetFeatureValue(CommonUsages.primaryButton, out primaryButtonValue) && primaryButtonValue)
                    {
                        uiSystem.SwitchScreen(skipTutorialStep);
                    }

                    if (!movementStepKeyText.Trim().Equals(""))
                    {
                        if (!skipTutorialStep.IsShowed())
                        {
                            taskUiLanguageTextTitle.ChangeText(movementStepKeyTitle, true);
                            taskUiLanguageTextText.ChangeText(movementStepKeyText, true);
                            taskUiLanguageTextNote.ChangeText(movementStepKeyNote, true);
                            uiSystem.SwitchScreen(taskUiScreen);
                        }

                        Vector3 currentPosition = vrCamera.transform.position;

                        if (Vector3.Distance(new Vector3(positionStep0.x, 0.0f, positionStep0.z), new Vector3(currentPosition.x, 0.0f, currentPosition.z)) > movementDistance)
                        {
                            stepID++;

                            taskUiLanguageTextTitle.ChangeText(teleportStepKeyTitle, true);
                            taskUiLanguageTextText.ChangeText(teleportStepKeyText, true);
                            taskUiLanguageTextNote.ChangeText(teleportStepKeyNote, true);
                            uiSystem.SwitchScreen(taskUiScreen);

                            if (skipTutorialStep.IsShowed())
                            {
                                skipTutorialStep.CloseScreen();
                            }

                            if (leftRayTeleport)
                                leftRayTeleport.onSelectExit.AddListener(Teleported);
                            if (rightRayTeleport)
                                rightRayTeleport.onSelectExit.AddListener(Teleported);
                        }
                    }
                    else
                        stepID++;
                break;
                case 1:
                    if (!teleportStepKeyText.Trim().Equals("") && (leftRayTeleport || rightRayTeleport))
                    {
                        if (!waitingForTeleport)
                        {
                            stepID++;

                            SetTasks();
                            taskStepID = 0;

                            if (taskSteps.Count > 0)
                            {
                                if(!taskSteps[taskStepID].dontShowText)
                                {
                                    taskUiLanguageTextTitle.ChangeText(taskSteps[taskStepID].title, false);
                                    taskUiLanguageTextText.ChangeText(taskSteps[taskStepID].text, false);
                                    taskUiLanguageTextNote.ChangeText(taskSteps[taskStepID].note, false);
                                    uiSystem.SwitchScreen(taskUiScreen);
                                }
                                else
                                    uiSystem.CloseAllScreen();

                                ruleBehaviour.onTaskCompleted.AddListener(TaskCompleted);
                            }
                        }
                    }
                    else
                        stepID++;
                break;
                case 2:
                    if (taskSteps.Count > 0)
                    {
                        interactOnlyInUI = false;
                        RayInteractionOnlyInUI(false);
                        if (!waitingForTask[taskStepID])
                        {
                            taskStepID++;

                            if (taskStepID >= taskSteps.Count)
                                stepID++;
                            else
                            {
                                if (!taskSteps[taskStepID].dontShowText)
                                {
                                    taskUiLanguageTextTitle.ChangeText(taskSteps[taskStepID].title, false);
                                    taskUiLanguageTextText.ChangeText(taskSteps[taskStepID].text, false);
                                    taskUiLanguageTextNote.ChangeText(taskSteps[taskStepID].note, false);
                                    uiSystem.SwitchScreen(taskUiScreen);
                                }
                                else
                                    uiSystem.CloseAllScreen();
                            }
                        }
                    }
                    else
                        stepID++;
                break;
                case 3:
                    interactOnlyInUI = true;
                    stepID++;

                    if (uiTaskScreen)
                        uiSystem.SwitchScreen(uiTaskScreen);
                    else
                        if (finishTutorialScreen)
                            uiSystem.SwitchScreen(finishTutorialScreen);
                    break;
            }

        }

        private void SetTasks()
        {
            foreach(TaskObjectTutorial task in taskSteps)
            {
                task.Completed = false;
                waitingForTask.Add(true);
            }
        }

        public void Refresh()
        {
            uiSystem.CloseAllScreen();

            uiSystem.SwitchScreen(taskUiScreen);
        }

        private void TaskCompleted(TaskBehaviour task)
        {
            if (task.task = taskSteps[taskStepID])
                waitingForTask[taskStepID] = false;

            if (taskStepID >= taskSteps.Count)
                ruleBehaviour.onTaskCompleted.RemoveListener(TaskCompleted);
        }

        private void Teleported(XRBaseInteractable interactable)
        {
            waitingForTeleport = false;

            if (leftRayTeleport)
                leftRayTeleport.onSelectExit.RemoveListener(Teleported);
            if (rightRayTeleport)
                rightRayTeleport.onSelectExit.RemoveListener(Teleported);
        }

        public void LoadScene() => SceneManager.LoadScene(1);

        public void TaskError(CheckTaskResult result)
        {
            if (messageScreen)
            {
                MessagesUI messageUi = messageScreen.GetComponent<MessagesUI>();
                messageUi.CleanMessage();
                messageUi.ShowError(LanguageSystem.Instance.GetValue("common.error"), result.Error);
                uiSystem.SwitchScreen(messageScreen);
            }

            Debug.Log("[Event] Task error event handled");
        }

        public void ShowQuestion(RuleBehaviour rule, TaskBehaviour task)
        {
            Debug.Log("[Event] Show question event handled");

            //TODO: evitar hacer el removeall
            var questionBehaviour = questionScreen.GetComponent<QuestionListBehaviour>();
            questionBehaviour.onAnsweredQuestion.RemoveAllListeners();
            questionBehaviour.onAnsweredQuestion.AddListener(delegate
            {
                rule.CompleteTask(task);
                uiSystem.CloseAllScreen();
            });

            questionBehaviour.Display(task.task.question);

            uiSystem.SwitchScreen(questionScreen);
        }

    }
}