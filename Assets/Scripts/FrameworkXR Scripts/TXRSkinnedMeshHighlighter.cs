﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SkinnedMeshRenderer))]
public class TXRSkinnedMeshHighlighter : TXRHighlighter
{
    private SkinnedMeshRenderer _skinnedmeshRenderer;

    void Start()
    {
        _skinnedmeshRenderer = GetComponent<SkinnedMeshRenderer>();
        _currentMaterial = _skinnedmeshRenderer.material;
        if (highlightMaterial)
        {
            highlightMaterial = Instantiate(highlightMaterial);
        }

        ToggleHighlight(false);
        highlightMaterial.SetTexture("Texture2D_722E04C9", _currentMaterial.mainTexture);
        highlightMaterial.SetColor("Color_E35AB720", Color.red);
    }

    protected override void ToggleHighlight(bool isOn)
    {
        _skinnedmeshRenderer.material = isOn ? highlightMaterial : _currentMaterial;
    }

}