﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace OT.Ternium.XR.Framework
{
    [Serializable]
    public class CarpetAnswerBehaviour : AnswerBehaviour
    {
        public Text classCode;
        public Text maxVolts;
        public GameObject background;
        public GameObject border;

        public override void Refresh()
        {
            var carpetAnswer = answer as CarpetAnswerObject;

            //GetComponent<AnswerButton>().number = carpetAnswer.id.ToString();
            if (background)
            {
                background.GetComponent<Image>().color = carpetAnswer.backgroundColor;
            }

            if (border)
            {
                border.GetComponent<Image>().color = carpetAnswer.backgroundColor;
            }

            this.classCode.text = $"CLASS {carpetAnswer.classCode}";
            this.maxVolts.text = $"{carpetAnswer.volts} VOLTS";
        }
    }
}