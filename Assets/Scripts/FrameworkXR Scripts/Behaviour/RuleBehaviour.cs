﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace OT.Ternium.XR.Framework
{
    [Serializable]
    public class StepCompletedEvent : UnityEvent<StepObject> { }

    [Serializable]
    public class TaskErrorEvent : UnityEvent<CheckTaskResult> { }

    [Serializable]
    public class ShowQuestionEvent : UnityEvent<RuleBehaviour, TaskBehaviour> { }

    [Serializable]
    public class TaskCompletedEvent : UnityEvent<TaskBehaviour> { }
    public class RuleBehaviour : MonoBehaviour
    {
        [SerializeField]
        private RuleManager _ruleManager;
        public StepCompletedEvent onStepCompleted;
        public TaskCompletedEvent onTaskCompleted;
        public TaskErrorEvent onTaskError;
        public UnityEvent onAllStepsCompleted;
        public ShowQuestionEvent onShowQuestion;

        void Start()
        {
            var tasks = GetComponentsInChildren<TaskBehaviour>(true);
            foreach (var task in tasks)
            {
                task.onSelected.AddListener(CheckTask);
                task.onError.AddListener(TaskError);
            }
        }

        public bool CheckTaskAnswer(TaskBehaviour task)
        {
            return _ruleManager.CheckTask(task.task).Success;
        }

        public bool CheckTaskStep(TaskObject task)
        {
            return _ruleManager.StepValid(task);
        }

        private void CheckTask(TaskBehaviour task)
        {
            CheckTaskResult result = _ruleManager.CheckTask(task.task);

            if (result.Success)
            {
                if (task.task.question)
                {
                    task.interactionController.SetBlocked(true);
                    onShowQuestion.Invoke(this, task);
                    return;
                }

                CompleteTask(task);
            }
            else
            {
                task.interactionController.SetBlocked(true);
                onTaskError.Invoke(result);
            }
        }

        public void CompleteTask(TaskBehaviour task)
        {
            CheckTaskResult result = task.interactionController.CanInteractWith();
            if (result.Success)
            {
                result = _ruleManager.CompleteTask(task.task);

                if (result.Success)
                {
                    onTaskCompleted.Invoke(task);

                    if (_ruleManager.AreAllStepsCompleted())
                    {
                        onAllStepsCompleted.Invoke();
                    }
                    else if (_ruleManager.IsCurrentStepCompleted())
                    {
                        onStepCompleted.Invoke(_ruleManager.GetCurrentStep());
                        _ruleManager.NextStep();
                    }

                    task.Compeletd();
                }
                else
                {
                    onTaskError.Invoke(result);
                }
            }
            else
            {
                task.interactionController.SetBlocked(true);
                onTaskError.Invoke(result);
            }
        }

        private void TaskError(TaskBehaviour task, string message)
        {
            CheckTaskResult result = new CheckTaskResult { Success = false, Error = message };
            onTaskError.Invoke(result);
        }
    }
}