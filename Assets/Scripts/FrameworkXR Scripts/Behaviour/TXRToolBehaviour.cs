﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{
    public class TXRToolBehaviour : XRGrabInteractable
    {

        [Header("TXR Werable")]
        public bool offsetGrab = false;     // The object use a offset grab?
        public bool holdToGrab = false;     // Hold the button to grab?
        public GameObject rightHandModel;   // Right Hand Model in Grab
        public GameObject leftHandModel;    // Left Hand Model in Grab
        public TXRInteractionController interactionController;

        [NonSerialized]
        public TXRRayInteractor interactorActive = null;  // Interactor Active

        private Vector3 _initialAttachLocalPos;        // Initial Attach Local Position
        private Quaternion _initialAttachLocalRot;     // Initial Attach Local Rotation

        void Start()
        {

            interactionController.onAction.AddListener(OnSelectEnter);
            interactionController.onExitAction.AddListener(OnSelectExit);

            // If there is no a Attach Tranform setted
            if (!attachTransform)
            {

                // Is created a pivot point for grabbing
                GameObject grabPivot = new GameObject("Grab Pivot");
                grabPivot.transform.SetParent(transform, false);
                attachTransform = grabPivot.transform;

            }

            // The initial attach tranform is saved
            _initialAttachLocalPos = attachTransform.localPosition;
            _initialAttachLocalRot = attachTransform.localRotation;

        }

        public void InvokeOnSelectEnter(XRBaseInteractor interactor)
        {
            OnSelectEnter(interactor);
        }

        protected override void OnSelectEnter(XRBaseInteractor interactor)
        {
            // If the offset grab is used
            if (offsetGrab)
            {

                // The attach tranform is setted
                if (interactor is XRDirectInteractor)
                {
                    attachTransform.position = interactor.transform.position;
                    attachTransform.rotation = interactor.transform.rotation;
                }
                else
                {
                    attachTransform.localPosition = _initialAttachLocalPos;
                    attachTransform.localRotation = _initialAttachLocalRot;
                }

            }

            base.OnSelectEnter(interactor);

            if (!(interactorActive is null))
            {
                interactorActive.dontKeepInHand = true;
                interactorActive.Drop();
                interactorActive.interactableActive = null;
                interactorActive = null;
            }

            if ((interactor is TXRRayInteractor || interactor is XRDirectInteractor) && (interactor.gameObject.CompareTag("Left Hand") || interactor.gameObject.CompareTag("Right Hand")))
            {
                if (!holdToGrab)
                {

                    if (!(interactorActive is null))
                    {
                        interactorActive.dontKeepInHand = true;
                        interactorActive.Drop();
                        interactorActive.interactableActive = null;
                        interactorActive = null;
                    }

                }

                if ((interactor is TXRRayInteractor || interactor is XRDirectInteractor) && (interactor.gameObject.CompareTag("Left Hand") || interactor.gameObject.CompareTag("Right Hand")))
                {

                    if (leftHandModel)
                    {
                        if (interactor.gameObject.CompareTag("Left Hand"))
                            interactor.gameObject.GetComponent<TXRHandPoseController>().SetHandPose(leftHandModel);
                    }

                    if (rightHandModel)
                    {
                        if (interactor.gameObject.CompareTag("Right Hand"))
                            interactor.gameObject.GetComponent<TXRHandPoseController>().SetHandPose(rightHandModel);
                    }

                    if (!holdToGrab)
                    {
                        if (interactor is TXRRayInteractor)
                        {
                            interactor.gameObject.GetComponent<TXRRayInteractor>().dontKeepInHand = false;
                            interactor.gameObject.GetComponent<TXRRayInteractor>().interactableActive = this;
                        }

                        interactorActive = (TXRRayInteractor)interactor;

                    }

                }
            }

        }

        protected override void OnSelectExit(XRBaseInteractor interactor) { 
        
                if (holdToGrab)
                    base.OnSelectExit(interactor);

                if (rightHandModel || leftHandModel)
                {
                    if ((interactor is TXRRayInteractor || interactor is XRRayInteractor) && (interactor.gameObject.CompareTag("Left Hand") || interactor.gameObject.CompareTag("Right Hand")))
                        interactor.gameObject.GetComponent<TXRHandPoseController>().UnSetHandPose();
                }

        }

        public void Drop()
        {

            if (!(interactorActive is null))
            {
                interactorActive.dontKeepInHand = true;
                interactorActive.interactableActive = null;
            }

            base.OnSelectExit(interactorActive);

            Rigidbody rb = gameObject.GetComponent<Rigidbody>();
            rb.isKinematic = false;
            rb.useGravity = true;

            interactorActive.Drop();

            interactorActive = null;
        }

        public TXRRayInteractor GetInteractor()
        {
            return interactorActive;
        }

    }
}