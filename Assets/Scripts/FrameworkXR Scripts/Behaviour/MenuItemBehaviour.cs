﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// Clase que contiene el corportamiento de un item de el menu de accion
/// </summary>
namespace OT.Ternium.XR.Framework
{
    [Serializable]
    public class MenuItemBehaviour : AnswerBehaviour
    {
        public Image iconIncorrect;
        public Image iconCorrect;
        public Text text;

        /// <summary>
        /// Metodo que refresca los items del menu, colocando su color y imagen correcto
        /// </summary>
        public override void Refresh()
        {
            var menuItem = answer as ActionMenuItemObject;

            if (text)
            {
                text.color = menuItem.textColor;

                if (menuItem.iconSetted == menuItem.iconCorrect)
                {
                    iconCorrect.sprite = menuItem.iconCorrect;
                    iconCorrect.gameObject.SetActive(true);
                    text.color = Color.green;
                }
                else if (menuItem.iconSetted == menuItem.iconIncorrect)
                {
                    iconIncorrect.sprite = menuItem.iconIncorrect;
                    iconIncorrect.gameObject.SetActive(true);
                    text.color = Color.red;
                }
            }

            text.text = menuItem.title;
        }

        /// <summary>
        /// Metodo para cambiar la respuesta correcta en el menu de accion
        /// </summary>
        public override void Change()
        {
            var menuItem = answer as ActionMenuItemObject;

            if (!answer.isCorrect)
            {
                iconIncorrect.sprite = menuItem.iconIncorrect;
                iconIncorrect.gameObject.SetActive(true);
                text.color = Color.red;
            }
        }

    }
}