﻿using OT.Ternium.XR.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{
    [Serializable]
    public class TaskEvent : UnityEvent<TaskBehaviour> { }

    [Serializable]
    public class TaskFailEvent : UnityEvent<TaskBehaviour, string> { }

    public class TaskBehaviour : MonoBehaviour
    {
        public TXRInteractionController interactionController;
        public TaskObject task;
        public TaskEvent onSelected;
        public TaskFailEvent onError;

        void Awake()
        {
            task.selectedFalsePositive = false;
            task.selectedAheadOfTime = false;
            interactionController.onClick.AddListener(Click);
        }

        void Start()
        {
            if (task)
            {
                task.Completed = false;
                task.selectedAheadOfTime = false;
            }
        }

        public virtual void Click(XRBaseInteractor interactor)
        {
            Debug.Log($"Task click");

            if (task && !task.Completed && interactionController.GetTaskBehaviour() == this)
                onSelected.Invoke(this);

        }

        public virtual void Error(string message)
        {
            Debug.LogError($"Task error: {message}");

            if (task)
                if (task.ErrorMessage.Equals("default"))
                    onError.Invoke(this, message);
                else
                    onError.Invoke(this, message);

        }

        public void Compeletd()
        {
            interactionController.Action();
            interactionController.ExitAction();
        }

    }
}