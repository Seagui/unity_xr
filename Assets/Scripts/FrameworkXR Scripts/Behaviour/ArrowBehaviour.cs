﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase que contiene el comportamiento de una flecha
/// </summary>
public class ArrowBehaviour : MonoBehaviour
{

    Transform target;

    void Update()
    {
        if (target)
        {
            //Si hay un target, se mira a el en cada frame
            transform.LookAt(target.position);
        }
    }

    /// <summary>
    /// Metodo para setear el target de la flecha
    /// </summary>
    /// <param name="target"></param>
    public void Activate(Transform target)
    {
        this.target = target;
        gameObject.SetActive(true);
    }

    /// <summary>
    /// Metodo para desactivar la flecha
    /// </summary>
    public void Desactivate()
    {
        this.target = null;
        gameObject.SetActive(false);
    }

}