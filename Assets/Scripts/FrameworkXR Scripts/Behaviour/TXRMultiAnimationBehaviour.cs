﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{
    public class TXRMultiAnimationBehaviour : XRBaseInteractable
    {

        [Header("TXR Interactable")]
        public TXRInteractionController interactionController;

        [SerializeField]
        private Animator animator;
        [SerializeField]
        private List<string> animationTriggers;

        private int currentTrigger = 0;

        void Start()
        {
            interactionController.onAction.AddListener(OnSelectEnter);
            interactionController.onExitAction.AddListener(OnSelectExit);
        }

        protected override void OnSelectEnter(XRBaseInteractor interactor)
        {

        }

        protected override void OnSelectExit(XRBaseInteractor interactor)
        {

            animator.SetTrigger(animationTriggers[currentTrigger]);

            currentTrigger++;

        }

    }
}
