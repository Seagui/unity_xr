﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{
    public class TXRInteractableSocketBehaviour : XRBaseInteractable
    {

        [Header("TXR Socket")]
        public TXRToolBehaviour toSocketObject;
        public TXRInteractableSocketBehaviour otherSocket = null;

        public InteractableEvent onActionSocket;

        public bool putObject = true;
        public bool hovering = false;

        public TXRInteractionController interactionController;

        void Start()
        {
            interactionController.onAction.AddListener(OnSelectEnter);
            interactionController.onExitAction.AddListener(OnSelectExit);
        }

        protected override void OnSelectEnter(XRBaseInteractor interactor) { }

        protected override void OnSelectExit(XRBaseInteractor interactor)
        {

            if (!(toSocketObject.interactorActive is null))
            {

                if (!(otherSocket is null))
                {

                    if (otherSocket.hovering)
                    {
                        onActionSocket.Invoke(interactor);
                    }

                }

                else
                {

                    if (putObject)
                    {

                        toSocketObject.Drop();
                        toSocketObject.gameObject.GetComponent<Rigidbody>().useGravity = false;
                        toSocketObject.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                        toSocketObject.gameObject.transform.position = gameObject.transform.position;
                        toSocketObject.gameObject.transform.rotation = gameObject.transform.rotation;
                        toSocketObject.gameObject.layer = 0;
                    }

                }

            }

        }

        protected override void OnHoverEnter(XRBaseInteractor interactor)
        {

            if (interactor.GetComponent<TXRRayInteractor>().interactableActive == toSocketObject)
            {
                hovering = true;
            }

            base.OnHoverEnter(interactor);
        }

        protected override void OnHoverExit(XRBaseInteractor interactor)
        {
            if (interactor.GetComponent<TXRRayInteractor>().interactableActive == toSocketObject)
            {
                hovering = false;
            }

            base.OnHoverEnter(interactor);
        }

    }
}