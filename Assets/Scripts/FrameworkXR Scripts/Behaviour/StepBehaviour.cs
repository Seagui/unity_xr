﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace OT.Ternium.XR.Framework
{
    [Serializable]
    public class StepEvent : UnityEvent<StepBehaviour> { }

    public class StepBehaviour : MonoBehaviour
    {
        public StepObject step;
        public Text stepName;
        public StepEvent onSelected;

        // Start is called before the first frame update
        void Start()
        {
            if (step)
                Display(step);
        }

        // Update is called once per frame
        public virtual void Display(StepObject step)
        {
            //this.step = step;

            //Refresh();
        }

        public virtual void Refresh()
        {
            //this.stepName.text = step.name;
        }

        public void Click()
        {
            this.onSelected.Invoke(this);
        }
    }
}