﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{
    public class TXRDoubleToolPickerBehaviour : XRBaseInteractable
    {

        [Header("TXR Double Tool Picker")]
        public TXRInteractionController interactionController;

        public GameObject gameObjectR;
        public GameObject gameObjectL;
        public TXRRayInteractor interactorR;
        public TXRRayInteractor interactorL;
        public Transform parent;

        void Start()
        {
            gameObjectL.SetActive(false);
            gameObjectR.SetActive(false);

            interactionController.onAction.AddListener(OnSelectEnter);
            interactionController.onExitAction.AddListener(OnSelectExit);
        }

        protected override void OnSelectEnter(XRBaseInteractor interactor) { }

        protected override void OnSelectExit(XRBaseInteractor interactor)
        {
            gameObjectL.SetActive(true);
            gameObjectR.SetActive(true);

            gameObjectR.GetComponent<TXRToolBehaviour>().InvokeOnSelectEnter(interactorR);
            gameObjectL.GetComponent<TXRToolBehaviour>().InvokeOnSelectEnter(interactorL);

            gameObjectR.transform.SetParent(parent);
            gameObjectL.transform.SetParent(parent);

            Destroy(interactionController.gameObject);
        }

    }
}