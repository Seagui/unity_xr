﻿using OT.Ternium.XR.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Metodo que setea los item del menu de accion en su estado default
/// </summary>
public class ActionMenuBehaviour : MonoBehaviour
{
    public ActionMenuObject actionMenu;

    void Start()
    {
        actionMenu.nextAnswer = 0;

        foreach (ActionMenuItemObject mi in actionMenu.answersItemsInCorrectOrder)
        {
            mi.answered = false;
            mi.isCorrect = false;
            mi.textColor = Color.white;
            mi.iconSetted = mi.icon;
        }
    }

}
