﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace OT.Ternium.XR.Framework
{
    [Serializable]
    public class GloveAnswerBehaviour : AnswerBehaviour
    {
        public Text maxVolts;
        public Text classCode;
        public GameObject border;
        public Image icon;
        public Image iconRight;
        public Image iconWrong;

        public override void Refresh()
        {
            var gloveAnswer = answer as GloveAnswerObject;

            //GetComponent<AnswerButton>().number = gloveAnswer.id.ToString();

            if (icon)
                this.icon.sprite = gloveAnswer.icon;
            if (iconRight)
                this.iconRight.sprite = gloveAnswer.icon;
            if (iconWrong)
                this.iconWrong.sprite = gloveAnswer.icon;

            if (border)
            {
                border.GetComponent<Image>().color = gloveAnswer.backgroundColor;
            }
            //this.classCode.text = $"CLASS {gloveAnswer.classCode}";
            this.classCode.text = $"{gloveAnswer.classCode}";
            this.classCode.color = gloveAnswer.textColor;
            if (icon)
                this.icon.color = gloveAnswer.textColor;
            this.maxVolts.text = $"{gloveAnswer.maxUseVolt}V AC";

        }

        public override void Change()
        {
            if (!answer.isCorrect)
            {
                if (iconWrong)
                    iconWrong.gameObject.SetActive(true);
                classCode.color = Color.red;
            }
            else
            {
                if (iconRight)
                    iconRight.gameObject.SetActive(true);
                classCode.color = Color.green;
            }
        }

    }
}