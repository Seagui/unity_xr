﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{

    public class TXRGlovesBehaviour : XRBaseInteractable
    {

        [Header("TXR Gloves")]
        public TXRInteractionController interactionController;
        [SerializeField]
        private GameObject leftHand;
        [SerializeField]
        private GameObject rightHand;
        [SerializeField]
        private TXRHandPresence leftHandPresence;
        [SerializeField]
        private TXRHandPresence rightHandPresence;

        public InteractableEvent onActionEvent;

        void Start()
        {
            interactionController.onAction.AddListener(OnSelectEnter);
            interactionController.onExitAction.AddListener(OnSelectExit);
        }

        protected override void OnSelectEnter(XRBaseInteractor interactor)
        {

            leftHandPresence.ChangeHandModel(leftHand);
            rightHandPresence.ChangeHandModel(rightHand);

            onActionEvent.Invoke(interactor);

        }

        protected override void OnSelectExit(XRBaseInteractor interactor) => Destroy(interactionController.gameObject);


    }

}