﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace OT.Ternium.XR.Framework
{
    [Serializable]
    public class AnswerEvent : UnityEvent<AnswerBehaviour> { }

    [RequireComponent(typeof(Button))]
    public class AnswerBehaviour : MonoBehaviour
    {
        public AnswerObject answer;
        // public Text answerName;
        public AnswerEvent onSelected;

        //TODO:A sacar solo para la muestra del miercoles 2-9
        //public TESSA.RiskPerception.UI.AnswerButton answerButton;

        // Start is called before the first frame update
        void Start()
        {

            //answerButton = GetComponent<TESSA.RiskPerception.UI.AnswerButton>();
            GetComponent<Button>().onClick.AddListener(Click);

            if (answer)
                Display(answer);
        }

        // Update is called once per frame
        public virtual void Display(AnswerObject answer)
        {
            this.answer = answer;
            Refresh();
        }

        public virtual void Refresh()
        {
            // this.answerName.text = answer.name;
        }

        public void Click()
        {
            //TODO: Sacar la utilizacion de answerButton
            /*
            if (answerButton != null)
            {
                answerButton.isCorrect = answer.isCorrect;
            }
            */
            Change();

            this.onSelected.Invoke(this);
        }

        public virtual void Change() { }

    }
}