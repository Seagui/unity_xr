﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{

    public class TXRAreaColliderBehaviour : XRBaseInteractable
    {

        [Header("TXR Werable")]
        public TXRInteractionController interactionController;

        public InteractableEvent onActionEvent;

        void Start()
        {
            interactionController.onAction.AddListener(OnSelectEnter);
            interactionController.onExitAction.AddListener(OnSelectExit);
        }

        protected override void OnSelectEnter(XRBaseInteractor interactor)
        {
        }

        protected override void OnSelectExit(XRBaseInteractor interactor)
        {
            onActionEvent.Invoke(interactor);
        }

    }

}