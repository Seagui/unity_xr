﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{
    public class TXRClimbInteractable : TXRWerableBehaviour
    {

        protected override void OnSelectEnter(XRBaseInteractor interactor)
        {

            base.OnSelectEnter(interactor);

            if(interactor is XRDirectInteractor)
                TXRClimber.climbingHand = interactor.GetComponent<XRController>();

        }

        protected override void OnSelectExit(XRBaseInteractor interactor)
        {

            base.OnSelectExit(interactor);

            if (interactor is XRDirectInteractor)
                if (TXRClimber.climbingHand && TXRClimber.climbingHand.name == interactor.name)
                    TXRClimber.climbingHand = null;

        }

    }
}