﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{

    public class TXRInteractableBehaviour : XRBaseInteractable
    {

        [Header("TXR Interactable")]
        public TXRInteractionController interactionController;

        [SerializeField]
        private Transform newTransform = null;
        [SerializeField]
        private GameObject newObject = null;
        [SerializeField]
        private bool animated = false;

        public InteractableEvent onActionEvent;

        private bool used = false;

        void Start()
        {
            interactionController.onAction.AddListener(OnSelectEnter);
            interactionController.onExitAction.AddListener(OnSelectExit);
        }

        protected override void OnSelectEnter(XRBaseInteractor interactor) {

        }

        protected override void OnSelectExit(XRBaseInteractor interactor) {

            if (interactionController.GetObjectBehaviour() == this)
            {

                if (!newTransform && !newObject && !animated)
                {
                    gameObject.SetActive(false);
                }

                if (!used)
                {

                    if (newTransform)
                    {
                        transform.position = newTransform.position;
                        transform.rotation = newTransform.rotation;
                    }

                    if (newObject)
                    {
                        transform.localScale = new Vector3(1f, 1f, 1f);
                        Instantiate(newObject, transform.position, transform.rotation, null);
                        gameObject.SetActive(false);
                    }

                    used = true;

                }

                onActionEvent.Invoke(interactor);

            }

        }

    }

}