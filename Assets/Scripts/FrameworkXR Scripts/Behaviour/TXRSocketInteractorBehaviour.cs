﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{

    public class TXRSocketInteractorBehaviour : XRSocketInteractor
    {

        public List<GameObject> permitedObjects;

        public override bool CanSelect(XRBaseInteractable interactable)
        {

            if (permitedObjects.Count == 0)
                return true;

            foreach (GameObject permitedObject in permitedObjects)
                if (interactable.gameObject == permitedObject)
                    return true;

            return false;

        }

    }

}