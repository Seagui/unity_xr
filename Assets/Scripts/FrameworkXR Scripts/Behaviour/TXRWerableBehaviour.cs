﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{

    public class TXRWerableBehaviour : XRBaseInteractable
    {

        [Header("TXR Werable")]
        public TXRInteractionController interactionController;

        [SerializeField]
        private XRSocketInteractor socket;

        void Start()
        {
            interactionController.onAction.AddListener(OnSelectEnter);
            interactionController.onExitAction.AddListener(OnSelectExit);
        }

        protected override void OnSelectEnter(XRBaseInteractor interactor)
        {

            interactionController.gameObject.transform.SetParent(socket.transform);
            interactionController.gameObject.transform.position = socket.transform.position;
            interactionController.gameObject.transform.rotation = socket.transform.rotation;

        }

        protected override void OnSelectExit(XRBaseInteractor interactor) { }

    }

}