﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace OT.Ternium.XR.Framework
{
    public class UI_System : MonoBehaviour
    {
        public InteractionOnlyInUI gameLogic;
        private List<UI_Screen> _screens;
        private UI_Screen _currentScreen;

        void Awake()
        {
            _screens = GetComponentsInChildren<UI_Screen>(true).ToList();
        }

        public void CloseAllScreen()
        {
            if (_screens != null)
                _screens.ForEach(screen =>
                {
                    screen.CloseScreen();
                    screen.enabled = false;
                    _currentScreen = null;
                });
            gameLogic.RayInteractionOnlyInUI(false);
        }

        public void CloseCurrentScreen()
        {
            if (_currentScreen)
            {
                _currentScreen.CloseScreen();
                _currentScreen.enabled = false;
                _currentScreen = null;
                gameLogic.RayInteractionOnlyInUI(false);
            }
        }

        public void SwitchScreen(UI_Screen screen)
        {
            if (screen)
            {
                if (_currentScreen)
                {
                    _currentScreen.CloseScreen();
                    _currentScreen.enabled = false;
                    _currentScreen = null;
                }
                _currentScreen = screen;
                _currentScreen.gameObject.SetActive(true);
                _currentScreen.enabled = true;
                _currentScreen.StartScreen();
                gameLogic.RayInteractionOnlyInUI(true);
            }
        }

    }
}