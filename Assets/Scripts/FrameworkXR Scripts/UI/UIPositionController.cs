﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{
    public class UIPositionController : MonoBehaviour
    {
        public Transform canvas;
        [Header("Root rotation")]
        public float maxAngle = 30;
        public float rotationSpeed = 2;
        public float recenterAngle = 0;
        [Header("Root position")]
        public float maxRootDistance = .25f;
        public float repositionSpeed = 2;
        [Header("Canvas height")]
        public float canvasHeightMaxDistance = .25f;
        public float canvasHeightRepositionSpeed = 2;

        [SerializeField]
        private XRRig _Rig;
        [SerializeField]
        private GameObject _PointToFollow;

        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            if (_Rig == null) return;
            Transform hdm = _PointToFollow.transform;
            Vector3 feetPosition = new Vector3(_PointToFollow.transform.position.x, _Rig.transform.position.y, _PointToFollow.transform.position.z);
            float eyeHeight = _Rig.cameraInRigSpaceHeight;

            UpdateRotation(hdm);
            UpdatePosition(feetPosition);
            UpdateCanvasHeight(eyeHeight);
        }

        private void UpdateRotation(Transform hdm)
        {
            Vector3 hdmForward = new Vector3(hdm.forward.x, 0, hdm.forward.z);
            float angle = Vector3.Angle(hdmForward, transform.forward);

            if (angle > maxAngle)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, hdm.eulerAngles.y, 0), Time.deltaTime * rotationSpeed);
            }
        }

        public void UpdatePosition(Vector3 feetPosition)
        {
            transform.position = new Vector3(transform.position.x, feetPosition.y, transform.position.z);
            if (Vector3.Distance(transform.position, feetPosition) > maxRootDistance)
            {
                transform.position = Vector3.Lerp(transform.position, feetPosition, Time.deltaTime * repositionSpeed);
            }
        }

        public void UpdateCanvasHeight(float eyeHeight)
        {
            if (Mathf.Abs(eyeHeight - canvas.localPosition.y) > canvasHeightMaxDistance)
            {
                canvas.localPosition = new Vector3(canvas.localPosition.x, Mathf.Lerp(canvas.localPosition.y, eyeHeight, Time.deltaTime * canvasHeightRepositionSpeed), canvas.localPosition.z);
            }
        }

        public void Recenter()
        {
            if (_Rig == null) return;
            Transform hdm = _PointToFollow.gameObject.transform;
            Vector3 feetPosition = new Vector3(_PointToFollow.transform.position.x, _Rig.transform.position.y, _PointToFollow.transform.position.z);
            transform.rotation = Quaternion.Euler(0, hdm.eulerAngles.y + recenterAngle, 0);
            transform.position = feetPosition;
        }


    }
}