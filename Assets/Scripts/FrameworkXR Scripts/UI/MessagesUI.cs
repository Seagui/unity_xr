﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace OT.Ternium.XR.Framework
{

    public class MessagesUI : MonoBehaviour
    {
        public Text title;
        public Image titleBackground;
        public Text messageContent;
        public Button closeBtn;

        public Color infoColor = Color.cyan;
        public Color successColor = Color.green;
        public Color warningColor = Color.yellow;
        public Color errorColor = Color.red;

        private Animator animator;
        private bool visible = false;
        private Queue<QueuedMessage> messagesQueue = new Queue<QueuedMessage>();

        public UnityEvent messagesClosed;

        private CanvasGroup canvasGroup;



        public bool isVisible
        {
            get { return visible; }
        }


        void Awake()
        {
            animator = GetComponentInChildren<Animator>();
            animator.SetBool("visible", false);
            visible = false;
            closeBtn.onClick.AddListener(Close);
            canvasGroup = GetComponent<CanvasGroup>();
        }

        private void Update()
        {
            canvasGroup.interactable = canvasGroup.blocksRaycasts = visible;
        }


        public void ShowInfo(string title, string text)
        {
            ShowMessage(title, text, infoColor);
        }

        public void ShowSuccess(string title, string text)
        {
            ShowMessage(title, text, successColor);
        }

        public void ShowWarning(string title, string text)
        {
            ShowMessage(title, text, warningColor);
        }

        public void ShowError(string title, string text)
        {
            ShowMessage(title, text, errorColor);
        }

        public void ShowMessage(string title, string text, Color color)
        {
            if (!visible)
            {
                this.title.color = color;
                this.title.text = title;
                this.titleBackground.color = color;
                this.messageContent.text = text;
                animator.SetBool("visible", true);
                visible = true;
            }
            else
            {
                Close();
            }
        }

        public void CleanMessage()
        {
            messagesQueue.Clear();
        }

        public void Close()
        {
            if (!visible) return;
            if (messagesQueue.Count > 0)
            {
                visible = false;
                QueuedMessage nextMessage = messagesQueue.Dequeue();
                ShowMessage(nextMessage.title, nextMessage.text, nextMessage.color);
            }
            else
            {
                visible = false;
                animator.SetBool("visible", false);
                messagesClosed.Invoke();
            }
        }


        private class QueuedMessage
        {
            public string title;
            public string text;
            public Color color;

            public QueuedMessage(string title, string text, Color color)
            {
                this.color = color;
                this.title = title;
                this.text = text;
            }
        }
    }
}
