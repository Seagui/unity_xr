﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace OT.Ternium.XR.Framework
{

    [RequireComponent(typeof(CanvasGroupAnimation))]
    public class QuestionsUI : MonoBehaviour
    {
        private static string questionIds = "ABCDEFGHIJKLMNOPGRSTUVWXYZ";

        public Text questionText;
        public GameObject answerPrefab;
        public Transform answersParent;

        private RiskData risk;

        private CanvasGroupAnimation _animation;
        private new CanvasGroupAnimation animation
        {
            get
            {
                if (_animation == null) _animation = GetComponent<CanvasGroupAnimation>();
                return _animation;
            }
        }

        public bool isVisible
        {
            get { return animation.isVisible; }
        }


        // Start is called before the first frame update
        void Start()
        {

        }


        public void Show(RiskData risk)
        {
            animation.Show();
            this.risk = risk;
            questionText.text = risk.question.text;
            CreateAnswers(risk.question.answers);
        }

        public void Hide()
        {
            animation.Hide();
        }

        private void CreateAnswers(RiskData.Answer[] answers)
        {
            ClearQuestions();

            for (int i = 0; i < answers.Length; i++)
            {
                int index = i;
                AnswerButton ans = Instantiate(answerPrefab, answersParent).GetComponent<AnswerButton>();
                ans.text = answers[i].text;
                ans.number = questionIds.ToCharArray()[i].ToString();
                ans.onClick.AddListener(
                    delegate
                    {
                        if (risk.questionAnswered) return;
                        ans.isCorrect = risk.CheckAnswer(index);
                    });
            }

        }

        private void ClearQuestions()
        {
            List<GameObject> destroy = new List<GameObject>();
            foreach (Transform t in answersParent)
                destroy.Add(t.gameObject);
            foreach (GameObject go in destroy)
                Destroy(go);
        }
    }
}