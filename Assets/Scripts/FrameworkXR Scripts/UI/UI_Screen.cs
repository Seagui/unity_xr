﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace OT.Ternium.XR.Framework
{
    [RequireComponent(typeof(CanvasGroupAnimation))]
    public class UI_Screen : MonoBehaviour
    {
        private CanvasGroupAnimation _canvasGroup;
        private bool isShowed;

        // Start is called before the first frame update
        void Awake()
        {
            _canvasGroup = GetComponent<CanvasGroupAnimation>();
        }

        public void Click()
        {
        }

        public void StartScreen()
        {
            if (_canvasGroup)
            {
                _canvasGroup.Show();
                isShowed = true;
            }
        }

        public void CloseScreen()
        {
            if (_canvasGroup)
            {
                _canvasGroup.Hide();
                isShowed = false;
            }
        }

        public bool IsShowed()
        {
            return gameObject.activeInHierarchy && isShowed && enabled;
        }
    }
}