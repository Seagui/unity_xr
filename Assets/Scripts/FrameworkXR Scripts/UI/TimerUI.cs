﻿using UnityEngine;
using UnityEngine.UI;

public class TimerUI : MonoBehaviour
{
    public Text minText;
    public Text secText;

    private bool count = true;

    [HideInInspector]
    public float time = 0;


    // Start is called before the first frame update
    void Start()
    {
        minText.text = "00";
        secText.text = "00";
    }

    public void StopTime()
    {
        count = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (count)
        {
            float min = Mathf.Floor(time / 60);
            float sec = Mathf.Floor(time - min * 60);
            minText.text = ((min < 10) ? "0" : "") + min;
            secText.text = ((sec < 10) ? "0" : "") + sec;
        }
    }

}
