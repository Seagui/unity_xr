﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace OT.Ternium.XR.Framework
{
    public class AnswerButton : MonoBehaviour
    {
        [SerializeField]
        private Text textLabel;
        [SerializeField]
        private Text numberLabel;
        [SerializeField]
        private Image[] colorFeedbackImages;
        [SerializeField]
        private Image correctImage;
        [SerializeField]
        private Image errorImage;

        public Color correctColor = Color.green;
        public Color incorrectColor = Color.red;

        private void Awake()
        {
            if (correctImage) correctImage.gameObject.SetActive(false);
            if (errorImage) errorImage.gameObject.SetActive(false);
        }

        public UnityEvent onClick
        {
            get { return GetComponent<Button>().onClick; }
        }

        public string text
        {
            set { textLabel.text = value; }
        }

        public string number
        {
            set { numberLabel.text = value; }
        }

        public bool isCorrect
        {
            set
            {
                Color color = value ? correctColor : incorrectColor;
                foreach(Image img in colorFeedbackImages)
                {
                    img.gameObject.SetActive(true);
                    img.color = color;
                }
                if (correctImage && value)
                {
                    correctImage.gameObject.SetActive(true);
                    correctImage.color = color;
                }
                if (errorImage && !value)
                {
                    errorImage.gameObject.SetActive(true);
                    errorImage.color = color;
                }
                
                numberLabel.gameObject.SetActive(false);

            }
        }
    }
}

