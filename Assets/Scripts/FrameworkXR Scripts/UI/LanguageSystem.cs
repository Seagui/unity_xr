﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageSystem : MonoBehaviour
{
    private static LanguageSystem _instance;
    public SystemLanguage _systemLanguage = SystemLanguage.Spanish;
    private Dictionary<string, string> _lang = new Dictionary<string, string>();

    public static LanguageSystem Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<LanguageSystem>();

                if (_instance == null)
                {
                    _instance = new GameObject().AddComponent<LanguageSystem>();
                }
            }

            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null) Destroy(this);
        DontDestroyOnLoad(this);
        LoadLanguage();
    }

    private void LoadLanguage()
    {
        // _systemLanguage = Application.systemLanguage;

        /*
        var file = Resources.Load<TextAsset>(_systemLanguage.ToString());

        if (file == null)
        {
            _systemLanguage = SystemLanguage.English;
            file = Resources.Load<TextAsset>(_systemLanguage.ToString());
        }

        foreach (var line in file.text.Split('\n'))
        {
            var prop = line.Split('=');
            _lang[prop[0]] = prop[1];
        }
        */

        TextAsset textAsset = Resources.Load(_systemLanguage.ToString()) as TextAsset;

        if (textAsset == null)
        {
            _systemLanguage = SystemLanguage.English;
            textAsset = Resources.Load(_systemLanguage.ToString()) as TextAsset;
        }

        foreach (var line in textAsset.text.Split('\n'))
        {
            var prop = line.Split('=');
            _lang[prop[0]] = prop[1];
        }
    }

    public string GetValue(string key, params string[] values)
    {
        string text = _lang[key];

        if(values != null)
        {
            for(int i = 0; i < values.Length; i++)
            {
                text = text.Replace($"{{{i}}}", values[i]);
            }
        }
        
        return text; 
    }
}
