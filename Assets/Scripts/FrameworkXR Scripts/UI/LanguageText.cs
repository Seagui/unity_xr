﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class LanguageText : MonoBehaviour
{
    public string key;
    void Start()
    {
        GetComponent<Text>().text = LanguageSystem.Instance.GetValue(key);
    }

    public void ChangeText(string newKey, bool key)
    {
        if (key)
            GetComponent<Text>().text = LanguageSystem.Instance.GetValue(newKey);
        else
            GetComponent<Text>().text = newKey;
    }
}
