﻿using System.Collections;
using UnityEngine;


[RequireComponent(typeof(CanvasGroup))]
public class CanvasGroupAnimation : MonoBehaviour
{

    private CanvasGroup _group;
    private CanvasGroup group
    {
        get
        {
            if (_group == null)
            {
                _group = GetComponent<CanvasGroup>();
                if (_group == null) _group = gameObject.AddComponent<CanvasGroup>();
            }
            return _group;
        }
    }

    public bool isVisible
    {
        get; private set;
    }

    private Coroutine coroutine;


    // Start is called before the first frame update
    void Start()
    {

    }


    public void Show(float time = .35f)
    {
        isVisible = true;
        if (coroutine != null) StopCoroutine(coroutine);
        group.interactable = true;
        group.blocksRaycasts = true;
        gameObject.SetActive(true);
        if (time <= 0) group.alpha = 1;
        else coroutine = StartCoroutine(AnimateAlpha(1, time));
    }

    public void Hide(float time = .35f)
    {
        isVisible = false;
        if (coroutine != null) StopCoroutine(coroutine);
        group.interactable = false;
        group.blocksRaycasts = false;
        gameObject.SetActive(true);
        if (time <= 0) group.alpha = 0;
        else coroutine = StartCoroutine(AnimateAlpha(0, time));
    }


    private IEnumerator AnimateAlpha(float alpha, float time)
    {
        float t = 0;
        float startVal = group.alpha;
        while (t < time)
        {
            group.alpha = Mathf.Lerp(startVal, alpha, t / time);
            t += Time.deltaTime;
            yield return null;
        }
        group.alpha = alpha;
    }


}
