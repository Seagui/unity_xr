﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using OT.Ternium.XR.Framework;

namespace OT.Ternium.XR.Framework
{
    public class GameLogic : InteractionOnlyInUI
    {

        public RuleBehaviour ruleBehaviour;
        public PlayerPicker playerPicker;
        public UI_System uiSystem;
        public UI_Screen welcomeScreen;
        public UI_Screen questionScreen;
        public UI_Screen messageScreen;
        public UI_Screen completedGameScreen;
        public TimerUI timer;
        public CriticalStepsUI criticalStepsUI;
        public float maxGameTime = 600f;
        public int livesCount = 10;
        public float secondsToShowEndScreen = 1f;

        public TXRHandPoseController leftHandPoseController;
        public TXRHandPoseController rightHandPoseController;

        public GameObject handDielectricRight;
        public GameObject handDielectricLeft;
        public GameObject handLeatherRight;
        public GameObject handLeatherLeft;
        public GameObject headProtection;

        public TaskEvent onQuestionAnswered;

        [NonSerialized]
        public static InputDevice headDevice;

        void Awake()
        {
            List<InputDevice> x = new List<InputDevice>();
            InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.TrackedDevice, x);
            if (x.Count > 0)
            {
                InputDevice _headDevice = x[0];
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            if (uiSystem)
            {
                uiSystem.CloseAllScreen();

                if (welcomeScreen)
                {
                    uiSystem.SwitchScreen(welcomeScreen);
                }
            }
            if (ruleBehaviour)
            {
                ruleBehaviour.onTaskCompleted.AddListener(TaskCompleted);
            }
            Debug.Log($"Lives Count = { livesCount }");
        }

        // Update is called once per frame
        void Update()
        {
            UpdateLeftTime();
        }

        void UpdateLeftTime()
        {
            maxGameTime -= Time.deltaTime;
            if (maxGameTime < 0)
            {
                GameOver();
            }
        }

        void GameOver()
        {
            Debug.LogWarning("Game Over");
            timer.StopTime();
            gameObject.SetActive(false);
        }

        public void StepCompleted(StepObject step)
        {
            Debug.Log("[Event] Step complete event handled");
        }

        public void ShowQuestion(RuleBehaviour rule, TaskBehaviour task)
        {
            Debug.Log("[Event] Show question event handled");

            CloseMessageUI();

            leftRayInteractor.InteractionLayerMask = LayerMask.GetMask("UI");
            leftRayInteractor.raycastMask = LayerMask.GetMask("UI");
            rightRayInteractor.InteractionLayerMask = LayerMask.GetMask("UI");
            rightRayInteractor.raycastMask = LayerMask.GetMask("UI");

            //TODO: evitar hacer el removeall
            var questionBehaviour = questionScreen.GetComponent<QuestionListBehaviour>();
            questionBehaviour.onAnsweredQuestion.RemoveAllListeners();
            questionBehaviour.onAnsweredQuestion.AddListener(delegate
            {
                onQuestionAnswered.Invoke(task);

                leftRayInteractor.InteractionLayerMask = interactionMask;
                leftRayInteractor.raycastMask = interactionMask;
                rightRayInteractor.InteractionLayerMask = interactionMask;
                rightRayInteractor.raycastMask = interactionMask;

                rule.CompleteTask(task);
                uiSystem.CloseAllScreen();
            });
            questionBehaviour.onAnswerIncorrect.AddListener(delegate
            {
                onQuestionAnswered.Invoke(task);

                LoseLives();
            });

            questionBehaviour.Display(task.task.question);

            uiSystem.SwitchScreen(questionScreen);
        }

        public void TaskCompleted(TaskBehaviour task)
        {

            switch (task.task.taskType)
            {

                default:
                    break;

            }

            CloseMessageUI();

        }

        public void TaskError(CheckTaskResult result)
        {
            CloseMessageUI();

            if (messageScreen)
            {
                MessagesUI messageUi = messageScreen.GetComponent<MessagesUI>();
                messageUi.CleanMessage();
                uiSystem.SwitchScreen(messageScreen);
                messageUi.ShowError(LanguageSystem.Instance.GetValue("common.error"), result.Error);
            }

            if (result.LoseLives)
            {
                LoseLives();
            }

            Debug.Log("[Event] Task error event handled");
        }

        private void LoseLives()
        {
            livesCount--;
            Debug.LogWarning($"Lives Count = { livesCount }");

            //if (livesCount == 0)
            //{
                //GameOver();
            //}
        }

        public void AllStepsCompleted()
        {
            CloseMessageUI();

            Debug.Log("[Event] All steps completed handled");

            if (uiSystem)
            {
                uiSystem.CloseAllScreen();

                if (completedGameScreen)
                {
                    StartCoroutine(Wait());
                }
            }

            timer.StopTime();
        }

        public void CloseMessageUI()
        {
            messageScreen.GetComponent<MessagesUI>().Close();
        }

        IEnumerator Wait()
        {
            yield return new WaitForSeconds(secondsToShowEndScreen);

            if (completedGameScreen)
            {
                uiSystem.SwitchScreen(completedGameScreen);
                criticalStepsUI.ShowCriticalSteps();
            }

            yield return 0;
        }

    }
}