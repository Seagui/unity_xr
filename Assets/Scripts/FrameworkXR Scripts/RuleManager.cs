﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace OT.Ternium.XR.Framework
{
    public class RuleManager : MonoBehaviour
    {

        public RuleObject rule;

        private int _stepIndex;
        private StepObject _currentStep;
        private StepObject _previousStep;

        // Start is called before the first frame update
        void Start()
        {
            Reset();
            NextStep();
        }

        public bool StepValid(TaskObject task)
        {
            return _currentStep.StepValid(task);
        }

        public CheckTaskResult CheckTask(TaskObject task)
        {
            if (!IsStepValid())
            {
                return new CheckTaskResult
                {
                    Success = false,
                    Error = task.ErrorMessage.Equals("default") ? LanguageSystem.Instance.GetValue("step.error.null") : task.ErrorMessage
                };
            }

            return _currentStep.CheckTask(task);
        }

        public CheckTaskResult CompleteTask(TaskObject task)
        {
            CheckTaskResult result = CheckTask(task);

            if (result.Success)
            {
                result = _currentStep.CompleteTask(task);

                if (result.Success)
                {
                    if (_currentStep.Completed)
                    {
                        Debug.Log($"The step {_currentStep.name} is completed");

                        if (AreAllStepsCompleted())
                        {
                            Debug.Log("All steps are completed");
                        }
                        else
                        {
                            NextStep();
                        }
                    }
                }
            }

            return result;
        }

        public StepObject GetCurrentStep()
        {
            return _currentStep;
        }
        public bool IsCurrentStepCompleted()
        {
            return IsStepValid() && _currentStep.Completed;
        }

        public bool AreAllStepsCompleted()
        {
            return !rule.steps.Exists(step => !step.Completed);
        }

        public bool IsStepValid()
        {
            if (!_currentStep)
            {
                Debug.LogError("The current step is null");
                return false;
            }

            return true;
        }

        private void Reset()
        {
            _currentStep = null;
            _previousStep = null;
            _stepIndex = 0;

            if (rule && rule.steps.Count > 0)
            {
                rule.steps.ForEach(step =>
                {
                    step.Completed = false;
                    step.tasks.ForEach(task => task.Completed = false);
                });
            }

            Debug.Log("The rule was reseted");
        }

        public void NextStep()
        {
            if (!rule)
            {
                Debug.LogError("The rule is null");
                return;
            }

            if (rule.steps.Count == 0)
            {
                Debug.LogError("The rule has not steps");
                return;
            }

            if (_currentStep)
            {
                _previousStep = _currentStep;
                Debug.Log($"Previous Step: { _previousStep.name }");
            }

            if (_stepIndex < rule.steps.Count)
            {
                _currentStep = rule.steps[_stepIndex++];
                Debug.Log($"Current Step: { _currentStep.name }");
            }
            else
            {
                Debug.LogWarning("No more steps");
            }
        }
    }
}