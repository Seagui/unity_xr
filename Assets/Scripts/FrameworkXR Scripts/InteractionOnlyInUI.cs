﻿using OT.Ternium.XR.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionOnlyInUI : MonoBehaviour
{
    public TXRRayInteractor leftRayInteractor;
    public TXRRayInteractor rightRayInteractor;
    public LayerMask interactionMask;

    protected bool interactOnlyInUI = true;

    public void RayInteractionOnlyInUI(bool onlyInUi)
    {
        if (interactOnlyInUI)
        {
            if (onlyInUi)
            {
                leftRayInteractor.InteractionLayerMask = LayerMask.GetMask("UI");
                leftRayInteractor.raycastMask = LayerMask.GetMask("UI");
                rightRayInteractor.InteractionLayerMask = LayerMask.GetMask("UI");
                rightRayInteractor.raycastMask = LayerMask.GetMask("UI");
            }
            else
            {
                leftRayInteractor.InteractionLayerMask = interactionMask;
                leftRayInteractor.raycastMask = interactionMask;
                rightRayInteractor.InteractionLayerMask = interactionMask;
                rightRayInteractor.raycastMask = interactionMask;
            }
        }
        else 
        {
            leftRayInteractor.InteractionLayerMask = interactionMask;
            leftRayInteractor.raycastMask = interactionMask;
            rightRayInteractor.InteractionLayerMask = interactionMask;
            rightRayInteractor.raycastMask = interactionMask;
        }
    }

}
