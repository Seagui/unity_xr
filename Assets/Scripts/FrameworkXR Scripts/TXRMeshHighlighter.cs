﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class TXRMeshHighlighter : TXRHighlighter
{
    private MeshRenderer _meshRenderer;

    void Start()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _currentMaterial = _meshRenderer.material;
        if (highlightMaterial)
        {
            highlightMaterial = Instantiate(highlightMaterial);
        }

        ToggleHighlight(false);
        highlightMaterial.SetTexture("Texture2D_722E04C9", _currentMaterial.mainTexture);
        highlightMaterial.SetColor("Color_E35AB720", Color.red);
    }

    protected override void ToggleHighlight(bool isOn)
    {
        _meshRenderer.material = isOn ? highlightMaterial : _currentMaterial;
    }

}