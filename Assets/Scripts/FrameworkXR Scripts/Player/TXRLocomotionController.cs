﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{
    public class TXRLocomotionController : MonoBehaviour
    {

        public XRRayInteractor leftInteractorRay, rightInteractorRay;
        public XRController leftTeleportRay, rightTeleportRay;
        public InputHelpers.Button teleportActivationButton;
        public float activationThreshold = 0.001f;

        public bool EnableLeftTeleport { get; set; } = true;
        public bool EnableRightTeleport { get; set; } = true;

        private XRRayInteractor _rightRayInteractor;
        private XRRayInteractor _leftRayInteractor;

        private void Start()
        {

            if (rightTeleportRay)
                _rightRayInteractor = rightTeleportRay.gameObject.GetComponent<XRRayInteractor>();
            if (leftTeleportRay)
                _leftRayInteractor = leftTeleportRay.gameObject.GetComponent<XRRayInteractor>();

        }

        void Update()
        {

            _leftRayInteractor.allowSelect = EnableLeftTeleport && CheckIfActivated(leftTeleportRay);
            leftTeleportRay.gameObject.SetActive(EnableLeftTeleport && CheckIfActivated(leftTeleportRay));

            _rightRayInteractor.allowSelect = EnableRightTeleport && CheckIfActivated(rightTeleportRay);
            rightTeleportRay.gameObject.SetActive(EnableRightTeleport && CheckIfActivated(rightTeleportRay));

            if (leftTeleportRay.gameObject.activeSelf || rightTeleportRay.gameObject.activeSelf)
            {
                leftInteractorRay.allowSelect = false;
                leftInteractorRay.allowHover = false;

                rightInteractorRay.allowSelect = false;
                rightInteractorRay.allowHover = false;
            }
            else
            {
                leftInteractorRay.allowSelect = true;
                leftInteractorRay.allowHover = true;

                rightInteractorRay.allowSelect = true;
                rightInteractorRay.allowHover = true;
            }


        }
        public bool CheckIfActivated(XRController controller)
        {

            InputHelpers.IsPressed(controller.inputDevice, teleportActivationButton, out bool isActivated, activationThreshold);
            return isActivated;

        }

    }
}