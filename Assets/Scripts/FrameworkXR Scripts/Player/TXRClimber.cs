﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{

    [RequireComponent(typeof(TXRContinousMovement))]
    public class TXRClimber : MonoBehaviour
    {
        public static XRController climbingHand;

        private CharacterController _character;      
        private TXRContinousMovement _continousMovement;

        void Start()
        {

            _character = GetComponent<CharacterController>();
            _continousMovement = GetComponent<TXRContinousMovement>();

        }

        void FixedUpdate()
        {

            _continousMovement.enabled = !climbingHand;

            if (climbingHand)
                Climb();

        }

        void Climb()
        {

            InputDevices.GetDeviceAtXRNode(climbingHand.controllerNode).TryGetFeatureValue(CommonUsages.deviceVelocity, out Vector3 velocity);

            _character.Move(transform.rotation * -velocity * Time.fixedDeltaTime);

        }

    }
}