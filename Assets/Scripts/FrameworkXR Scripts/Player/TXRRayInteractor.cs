﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


namespace OT.Ternium.XR.Framework
{

    public class TXRRayInteractor : XRRayInteractor
    {

        [System.NonSerialized]
        public bool dontKeepInHand = true;
        [System.NonSerialized]
        public TXRToolBehaviour interactableActive = null;

        public void Drop()
        {
            base.OnSelectExit(interactableActive);
        }

    }

}