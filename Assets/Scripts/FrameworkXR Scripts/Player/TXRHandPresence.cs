﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{
    public class TXRHandPresence : MonoBehaviour
    {

        public bool showController = false;
        public List<GameObject> controllerPrefabs;
        public GameObject handModelPrefab;
        public InputDeviceCharacteristics controllerCharacteristics;
        public bool settedHand = false;

        [SerializeField]
        private XRController InteractorRay;
        [SerializeField]
        private XRController TeleportRay;
        [SerializeField]
        private TXRLocomotionController LocomotionController;

        private InputDevice _targetDevice;
        private GameObject _spawnedController;
        private GameObject _spawnedHandModel;
        private Animator _handAnimator;

        void Start() => TryInitialize();

        void TryInitialize()
        {

            List<InputDevice> devices = new List<InputDevice>();
            InputDevices.GetDevicesWithCharacteristics(controllerCharacteristics, devices);

            if (devices.Count > 0)
                SetControl(devices[0].name);

            if (devices.Count > 0)
            {
                _targetDevice = devices[0];
                GameObject prefab = controllerPrefabs.Find(controller => controller.name == _targetDevice.name);
                if (prefab)
                {
                    _spawnedController = Instantiate(prefab, transform);
                }
                else
                {
                    Debug.LogError("Did not find corresponding controller model");
                    _spawnedController = Instantiate(controllerPrefabs[0], transform);
                }

                _spawnedHandModel = Instantiate(handModelPrefab, transform);
                _handAnimator = _spawnedHandModel.GetComponent<Animator>();
            }

        }

        void SetControl(string control)
        {
            Debug.Log("Control detectado: " + control);

            InteractorRay.selectUsage = InputHelpers.Button.Trigger;
            InteractorRay.activateUsage = InputHelpers.Button.Trigger;
            InteractorRay.uiPressUsage = InputHelpers.Button.Trigger;

            if (control.Contains("cosmos") || control.Contains("quest"))
            {
                TeleportRay.selectUsage = InputHelpers.Button.SecondaryButton;
                TeleportRay.activateUsage = InputHelpers.Button.SecondaryButton;
                TeleportRay.uiPressUsage = InputHelpers.Button.SecondaryButton;

                LocomotionController.teleportActivationButton = InputHelpers.Button.SecondaryButton;
            }
            else if (control.Contains("vive"))
            {
                TeleportRay.selectUsage = InputHelpers.Button.Primary2DAxisClick;
                TeleportRay.activateUsage = InputHelpers.Button.Primary2DAxisClick;
                TeleportRay.uiPressUsage = InputHelpers.Button.Primary2DAxisClick;

                LocomotionController.teleportActivationButton = InputHelpers.Button.Primary2DAxisClick;
            }
 
        }

        void UpdateHandAnimation()
        {

            if (_targetDevice.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue))
                _handAnimator.SetFloat("Trigger", triggerValue);
            else
                _handAnimator.SetFloat("Trigger", 0);

            if (_targetDevice.TryGetFeatureValue(CommonUsages.grip, out float gripValue))
                _handAnimator.SetFloat("Grip", gripValue);
            else
                _handAnimator.SetFloat("Grip", 0);

        }

        void Update()
        {
        
            if(!settedHand)
                if (!_targetDevice.isValid)
                    TryInitialize();

                else
                    if (showController)
                    {
                        _spawnedHandModel.SetActive(false);
                        _spawnedController.SetActive(true);
                    }

                    else
                    {
                        _spawnedHandModel.SetActive(true);
                        _spawnedController.SetActive(false);
                        if (_spawnedHandModel.GetComponent<Animation>())
                            UpdateHandAnimation();
                    }

        }

        public void ChangeHandModel(GameObject newHandModel)
        {

            Destroy(_spawnedHandModel);

            _spawnedHandModel = Instantiate(newHandModel, transform);

            if (_spawnedHandModel.GetComponent<Animation>())
                _handAnimator = _spawnedHandModel.GetComponent<Animator>();

        }

    }
}