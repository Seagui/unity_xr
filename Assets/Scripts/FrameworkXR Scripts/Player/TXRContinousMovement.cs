﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{
    public class TXRContinousMovement : MonoBehaviour
    {

        public XRNode inputSource;
        public LayerMask groundLayer;
        public float speed = 1f;
        public float gravity = -9.81f;
        public float additionalHeight = 0.2f;

        private CharacterController _character;
        private XRRig _rig;
        private Vector2 _inputAxis;
        private float _fallingSpeed;

        void Start()
        {

            _character = GetComponent<CharacterController>();
            _rig = GetComponent<XRRig>();

        }

        void Update()
        {

            InputDevice device = InputDevices.GetDeviceAtXRNode(inputSource);
            device.TryGetFeatureValue(CommonUsages.primary2DAxis, out _inputAxis);

        }

        void FixedUpdate()
        {

            CapsuleFollowHeadset();

            Quaternion headYaw = Quaternion.Euler(0, _rig.cameraGameObject.transform.eulerAngles.y, 0);
            Vector3 direction = headYaw * new Vector3(_inputAxis.x, 0, _inputAxis.y);

            _character.Move(direction * Time.deltaTime * speed);

            bool isGrounded = CheckIfGrounded();
            if (isGrounded)
                _fallingSpeed = 0;
            else
                _fallingSpeed += gravity * Time.fixedDeltaTime;

            _character.Move(Vector3.up * _fallingSpeed * Time.fixedDeltaTime);

        }

        void CapsuleFollowHeadset()
        {

            _character.height = _rig.cameraInRigSpaceHeight + additionalHeight;
            Vector3 capsuleCenter = transform.InverseTransformPoint(_rig.cameraGameObject.transform.position);
            _character.center = new Vector3(capsuleCenter.x, _character.height / 2 + _character.skinWidth, capsuleCenter.z);

        }

        bool CheckIfGrounded()
        {

            Vector3 rayStart = transform.TransformPoint(_character.center);
            float rayLenght = _character.center.y + 0.01f;
            bool hasHit = Physics.SphereCast(rayStart, _character.radius, Vector3.down, out _, rayLenght, groundLayer);
            return hasHit;
    
        }

    }
}