﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OT.Ternium.XR.Framework
{
    public class TXRHandPoseController : MonoBehaviour
    {

        public TXRHandPresence handPresence;

        private GameObject _spawnedHandPose = null;

        public void SetHandPose(GameObject model)
        {
            if (!(_spawnedHandPose is null))
            {
                UnSetHandPose();
            }

            handPresence.settedHand = true;

            _spawnedHandPose = Instantiate(model, transform);
        }

        public void UnSetHandPose()
        {
            handPresence.settedHand = false;

            _spawnedHandPose.SetActive(false);
            _spawnedHandPose = null;
        }


    }
}
