﻿public class CheckTaskResult
{
    public bool Success { get; set; }
    public bool LoseLives { get; set; }
    public string Error { get; set; }

    public CheckTaskResult()
    {
        Success = false;
        LoseLives = false;
        Error = string.Empty;
    }
}
