﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

namespace OT.Ternium.XR.Framework
{

    [Serializable]
    public class InteractableEvent : UnityEvent<XRBaseInteractor> { }

    public class TXRInteractionController : XRBaseInteractable
    {

        private bool blocked = false;               // Is the object blocked?
        private TXRRayInteractor interactor = null; // Interactor interactuanding with the task
        private int currentBehaviour = 0;           // Behaivour currently active
        private int currentTask = 0;

        [Header("TXR Interaction Controller")]
        public RuleBehaviour ruleBehaviour;
        public List<TaskBehaviour> taskBehaviours;
        public List<TXRHighlighter> highlighters;
        public bool canBeUsed = true;           // Can be used?
        public bool canInteractWithHand = true; // Can be used with only the hand?
        public bool showNegativeHiglighterInStep = false;
        public TaskObject imiteHiglighterOfTask = null;
        public string falsePositiveMessage = "";
        public bool actionInCollision = false;
        public bool actionInInteraction = true;
        public bool alwaysCollider = false;

        [SerializeField]
        private List<GameObject> permittedObject;           // List of permitted objects to interact with
        [SerializeField]
        private List<XRBaseInteractable> objectBehaviour;   // List of behaviours of the task

        [Header("TXR Interaction Controller Events")]
        public InteractableEvent onClick;
        public InteractableEvent onExit;

        public InteractableEvent onAction;
        public InteractableEvent onExitAction;

        void Start()
        {
            SetBlockedBehaviorus();

            if (imiteHiglighterOfTask)
                canBeUsed = false;
        }

        private void SetBlockedBehaviorus()
        {
            for (int i=0; i<objectBehaviour.Count; i++)
            {
                if (i == currentBehaviour)
                {
                    objectBehaviour[i].enabled = true;
                } 
                else
                {
                    objectBehaviour[i].enabled = false;
                }
            }
        }

        public CheckTaskResult CanInteractWith()
        {

            if (!(interactor is null))
            {
                if (!(interactor.interactableActive is null))
                {
                    bool permitted = false;
                    foreach (GameObject gameObject in permittedObject)
                    {
                        if (interactor.interactableActive.gameObject == gameObject)
                        {
                            permitted = true;
                        }
                    }

                    if (permitted)
                    {
                        return new CheckTaskResult { Success = true };
                    }
                    else
                    {
                        return new CheckTaskResult { Success = false };
                    }
                }
                else
                {
                    if (canInteractWithHand)
                    {
                        return new CheckTaskResult { Success = true };
                    }
                    else
                    {
                        return new CheckTaskResult { Success = false };
                    }
                }
            }
            else
            {
                if (canInteractWithHand)
                {
                    return new CheckTaskResult { Success = true };
                }
                else
                {
                    return new CheckTaskResult { Success = false };
                }
            }

        }

        protected override void OnSelectEnter(XRBaseInteractor interactor)
        {
            if (actionInInteraction)
            {
                if (taskBehaviours.Count > 0 || objectBehaviour.Count > 0)
                {
                    this.interactor = (TXRRayInteractor)interactor;

                    CompleteIn();

                    Action();
                }
            }

        }

        protected override void OnSelectExit(XRBaseInteractor interactor)
        {

            if (actionInInteraction)
            {
                if (taskBehaviours.Count > 0 || objectBehaviour.Count > 0)
                {
                    CompleteOut();

                    ExitAction();

                    SetBlockedBehaviorus();

                    blocked = false;

                    this.interactor = null;
                }
            }

        }

        protected void OnTriggerEnter(Collider other)
        {

            if (actionInCollision)
            {
                if (taskBehaviours.Count > 0 || objectBehaviour.Count > 0)
                {
                    CompleteIn();

                    Action();
                }
            }

        }

        protected void OnTriggerExit(Collider other)
        {

            if (actionInCollision)
            {
                if (taskBehaviours.Count > 0 || objectBehaviour.Count > 0)
                {
                    CompleteOut();

                    ExitAction();

                    SetBlockedBehaviorus();

                    blocked = false;
                }
            }

        }

        public void CompleteIn()
        {
            if (!imiteHiglighterOfTask && (currentBehaviour == objectBehaviour.Count - 1 || taskBehaviours.Count > 1))
            {
                onClick.Invoke(interactor);
            }
        }

        public void Action()
        {
            if (!blocked && canBeUsed)
            {
                onAction.Invoke(interactor);
            }
        }

        public void CompleteOut()
        {
            if (!imiteHiglighterOfTask && (currentBehaviour == objectBehaviour.Count - 1 || taskBehaviours.Count > 1))
            {           
                onExit.Invoke(interactor);
            }
        }

        public void ExitAction()
        {
            if (!blocked && canBeUsed)
            {
                onExitAction.Invoke(interactor);

                if (objectBehaviour.Count > 0)
                {
                    currentBehaviour++;

                    if (objectBehaviour.Count <= currentBehaviour)
                    {
                        gameObject.GetComponent<Collider>().enabled = false;
                    }
                }
                else
                {
                    gameObject.GetComponent<Collider>().enabled = false;
                }

            }

            if (!imiteHiglighterOfTask && (taskBehaviours.Count > 1 && taskBehaviours[currentTask].task.Completed))
            {
                currentTask++;
            }

            if (imiteHiglighterOfTask)
            {
                if (falsePositiveMessage.Trim().Equals(""))
                    taskBehaviours[currentTask].Error("La tarea " + taskBehaviours[currentTask].task.name + " no fue espeseficada en la Orden de Trabajo.");
                else
                    taskBehaviours[currentTask].Error(falsePositiveMessage);
                taskBehaviours[currentTask].task.SetSelected();
            }
        }

        public void CanBeUsed() {
            if (!imiteHiglighterOfTask)
                canBeUsed = !canBeUsed;
        }

        public void SetCanBeUsed(bool canBeUsed) => this.canBeUsed = canBeUsed;

        public void SetBlocked(bool blocked)
        {

            this.blocked = blocked;
            objectBehaviour[currentBehaviour].enabled = !blocked;

        }

        protected override void OnHoverEnter(XRBaseInteractor interactor)
        {
            base.OnHoverEnter(interactor);

            if (currentTask >= taskBehaviours.Count && currentBehaviour >= objectBehaviour.Count && !alwaysCollider)
                foreach (Collider collider in gameObject.GetComponents<Collider>())
                    collider.enabled = false;
            else
            {
                foreach (TXRHighlighter highlighter in highlighters)
                {
                    if (imiteHiglighterOfTask)
                        if (ruleBehaviour.CheckTaskStep(imiteHiglighterOfTask))
                        {
                            if (showNegativeHiglighterInStep)
                            {
                                if (blocked || !canBeUsed || !CanInteractWith().Success)
                                    highlighter.ChangeColor(true);
                                else
                                    highlighter.ChangeColor(false);
                            }
                            else
                                highlighter.ChangeColor(false);
                        }
                        else
                            highlighter.ChangeColor(true);
                    else
                    {
                        if (ruleBehaviour.CheckTaskAnswer(taskBehaviours[currentTask]))
                        {
                            if (showNegativeHiglighterInStep)
                            {
                                if (blocked || !canBeUsed || !CanInteractWith().Success)
                                    highlighter.ChangeColor(true);
                                else
                                    highlighter.ChangeColor(false);
                            }
                            else
                                highlighter.ChangeColor(false);
                        }
                        else
                            highlighter.ChangeColor(true);
                    }

                    highlighter.SelectEnter();
                }
            }

        }

        protected override void OnHoverExit(XRBaseInteractor interactor)
        {
            base.OnHoverExit(interactor);

            foreach (TXRHighlighter highlighter in highlighters)
                highlighter.SelectExit();

        }

        public TaskBehaviour GetTaskBehaviour()
        {
            return taskBehaviours[currentTask];
        }

        public XRBaseInteractable GetObjectBehaviour()
        {
            return objectBehaviour[currentBehaviour];
        }

    }
}